(function($) {
    'use strict';

    var MM = {

        toggleLuzes: function(event) {

            event.preventDefault();

            var _this  = $(this),
                iframe = $('.video-interna-player iframe');

            if (_this.hasClass('acender')) {

                $('.overlay').fadeOut('slow', function() {
                    $(this).remove();
                    iframe.removeClass('acima');
                });

                _this.removeClass('acender').text(_this.text().replace('acender', 'apagar'));

            } else {

                $('body').prepend('<div class="overlay"></div>');
                iframe.addClass('acima');
                $('.overlay').fadeIn('slow');

                _this.addClass('acender').text(_this.text().replace('apagar', 'acender'));

            }

        },

        enviaContato: function(event) {

            event.preventDefault();
            var form     = this,
                response = $('.contato-response'),
                loading  = $('.ajax-loading');

            response.hide();
            loading.fadeIn('slow');

            $.post('ajaxcontato', {

                nome     : $('#nome').val(),
                email    : $('#email').val(),
                telefone : $('#email').val(),
                mensagem : $('#mensagem').val()

            }, function(data) {

                form.reset();
                loading.fadeOut('fast', function() {
                    response.fadeIn('slow');
                });

            });

        },

        enviaNewsletter: function(event) {

            event.preventDefault();
            var form     = $(this),
                response = $('.newsletter-response');

            $.post('ajaxnewsletter', {

                nome  : $('#nome').val(),
                email : $('#email').val()

            }, function(data) {

                form.fadeOut('fast', function() {
                    response.fadeIn('slow');
                });

            });

        },

        selecionaCarousel: function(event) {

            event.preventDefault();
            var clicked      = $(this),
                videoWrapper = $('.video-wrapper'),
                currentData  = $('.carousel-dados');

            if (clicked.hasClass('active')) {
                return false;
            } else {
                var dados = clicked.data();
                $('.carousel-thumb').removeClass('active');
                clicked.addClass('active');

                videoWrapper.fadeOut('fast', function() {
                    videoWrapper.html('<iframe src="//player.vimeo.com/video/' + dados.videoid + '?color=f05a8e&portrait=0&autoplay=0&badge=0&title=0&byline=0" width="640" height="355" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
                    setTimeout(function(){videoWrapper.fadeIn('slow');}, 150);
                });

                currentData.fadeOut('fast', function() {
                    $('.data').text(dados.data);
                    $('.titulo').text(dados.titulo);
                    $('.descricao').text(dados.descricao);
                    currentData.fadeIn('fast');
                });

            }

        },

        init: function() {

            $('.luzes').on('click', MM.toggleLuzes);
            $('#contato-form').on('submit', MM.enviaContato);
            $('#newsletter-form').on('submit', MM.enviaNewsletter);
            $('.carousel-thumb').on('click', MM.selecionaCarousel);
            $('.carousel').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: false,
                touchMove: false,
                draggable: false,
                swipe: false
            });

        }

    };

    $(document).ready(MM.init);

}(jQuery));