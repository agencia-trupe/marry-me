<?php

if (isset($_GET['url'])) {

    $view = $_GET['url'];

    if (!is_file('views/'. $view .'.php')) {
        header('Location: http://'. $_SERVER["HTTP_HOST"] .'/marryme');
        exit;
    }

} else {

    $view = 'home';

}

if (isset($_GET['cat'])) {
    require 'views/_templates/header-categorias.php';
    require 'views/'. $view .'.php';
    require 'views/_templates/footer-categorias.php';
} else {
    require 'views/_templates/header.php';
    require 'views/'. $view .'.php';
    require 'views/_templates/footer.php';
}
