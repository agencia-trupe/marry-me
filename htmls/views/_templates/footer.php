    <footer>
        <div class="centro">
            <div class="left">
                <nav>
                    <a href="home">Home</a>
                    <a href="empresa">Nossa Empresa</a>
                    <a href="videos">Vídeos</a>
                    <a href="contato">Contato</a>
                </nav>
                <p class="copyright">©2014 Marry Me · Todos os direitos reservados</p>
            </div>
            <div class="right">
                <a href="#" class="facebook">Facebook</a>
                <a href="mailto:contato@marryme.com.br" class="email">contato@marryme.com.br</a>
                <p class="copyright">
                    <a href="http://www.trupe.net" target="_blank">Criação de Sites: </a>
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
    <script src="assets/js/main.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
</body>
</html>