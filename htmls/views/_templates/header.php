<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Marry Me</title>
    <meta name="description" content="Marry Me">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/main.min.css">
</head>
<body>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <header>
        <div class="centro">
            <a href="home" class="logo">Marry Me</a>
            <nav>
                <ul>
                    <li><a href="empresa"<?=(preg_match('/^empresa/', $view) ? ' class="active"' : '')?>>Nossa Empresa</a></li>
                    <li><a href="videos"<?=(preg_match('/^videos/', $view) ? ' class="active"' : '')?>>Vídeos</a></li>
                    <li><a href="contato"<?=(preg_match('/^contato/', $view) ? ' class="active"' : '')?>>Contato</a></li>
                </ul>
            </nav>
            <a href="#" class="facebook">Facebook</a>
            <form action="" class="busca">
                <input type="text" name="busca" placeholder="BUSCAR">
                <input type="submit" value>
            </form>
        </div>
    </header>
