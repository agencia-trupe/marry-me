<div class="categoria-titulo">
    <div class="centro">
        <img src="assets/img/categoria-exemplo-pequeno.png" alt="">
        <h2>Nome da Categoria</h2>
    </div>
</div>
<div class="centro">
    <div class="videos-categorias">
        <a href="videos-exemplo-c?cat" class="videos-thumb">
            <div class="video-thumb">
                <img src="assets/img/upload/thumb-302x182.jpg" alt="">
                <div class="overlay"></div>
            </div>
            <div class="video-descricao">
                <p>
                    <span>01 de agosto de 2014</span>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In feugiat mattis sem, auctor accumsan sapien.
                </p>
            </div>
        </a>
        <a href="videos-exemplo-c?cat" class="videos-thumb">
            <div class="video-thumb">
                <img src="assets/img/upload/thumb-302x182.jpg" alt="">
                <div class="overlay"></div>
            </div>
            <div class="video-descricao">
                <p>
                    <span>01 de agosto de 2014</span>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In feugiat mattis sem, auctor accumsan sapien.
                </p>
            </div>
        </a>
        <a href="videos-exemplo-c?cat" class="videos-thumb">
            <div class="video-thumb">
                <img src="assets/img/upload/thumb-302x182.jpg" alt="">
                <div class="overlay"></div>
            </div>
            <div class="video-descricao">
                <p>
                    <span>01 de agosto de 2014</span>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In feugiat mattis sem, auctor accumsan sapien.
                </p>
            </div>
        </a>
        <a href="videos-exemplo-c?cat" class="videos-thumb">
            <div class="video-thumb">
                <img src="assets/img/upload/thumb-302x182.jpg" alt="">
                <div class="overlay"></div>
            </div>
            <div class="video-descricao">
                <p>
                    <span>01 de agosto de 2014</span>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In feugiat mattis sem, auctor accumsan sapien.
                </p>
            </div>
        </a>
        <a href="videos-exemplo-c?cat" class="videos-thumb">
            <div class="video-thumb">
                <img src="assets/img/upload/thumb-302x182.jpg" alt="">
                <div class="overlay"></div>
            </div>
            <div class="video-descricao">
                <p>
                    <span>01 de agosto de 2014</span>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In feugiat mattis sem, auctor accumsan sapien.
                </p>
            </div>
        </a>
        <a href="videos-exemplo-c?cat" class="videos-thumb">
            <div class="video-thumb">
                <img src="assets/img/upload/thumb-302x182.jpg" alt="">
                <div class="overlay"></div>
            </div>
            <div class="video-descricao">
                <p>
                    <span>01 de agosto de 2014</span>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In feugiat mattis sem, auctor accumsan sapien.
                </p>
            </div>
        </a>
        <a href="videos-exemplo-c?cat" class="videos-thumb">
            <div class="video-thumb">
                <img src="assets/img/upload/thumb-302x182.jpg" alt="">
                <div class="overlay"></div>
            </div>
            <div class="video-descricao">
                <p>
                    <span>01 de agosto de 2014</span>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In feugiat mattis sem, auctor accumsan sapien.
                </p>
            </div>
        </a>
        <a href="videos-exemplo-c?cat" class="videos-thumb">
            <div class="video-thumb">
                <img src="assets/img/upload/thumb-302x182.jpg" alt="">
                <div class="overlay"></div>
            </div>
            <div class="video-descricao">
                <p>
                    <span>01 de agosto de 2014</span>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In feugiat mattis sem, auctor accumsan sapien.
                </p>
            </div>
        </a>
        <div class="videos-categorias-paginacao">
            <a href="#" class="anteriores">vídeos anteriores</a>
            <nav class="paginacao">
                <a href="#" class="active">1</a>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">4</a>
                <a href="#">5</a>
            </nav>
            <a href="#" class="proximos">próximos vídeos</a>
        </div>
    </div>
    <aside class="videos-categorias-aside">
        <div class="categorias">
            <h3>Categorias</h3>
            <a href="#">Nome da Categoria</a>
            <a href="#" class="active">Nome da Categoria</a>
            <a href="#">Nome da Categoria</a>
            <a href="#">Nome da Categoria</a>
            <a href="#">Nome da Categoria</a>
            <a href="#">Nome da Categoria</a>
        </div>
    </aside>
</div>