<div class="video-interna-player">
    <div class="centro">
        <iframe src="//player.vimeo.com/video/110566773?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0" width="940" height="530" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay="0"></iframe>
    </div>
</div>
<div class="video-interna-faixa">
    <div class="centro">
        <a href="#" class="luzes">Clique para apagar as luzes!</a>
        <div class="compartilhe">
            <p>compartilhe:</p>
            <a href="#" class="facebook">facebook</a>
            <a href="#" class="twitter">twitter</a>
            <a href="#" class="pinterest">pinterest</a>
            <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true"></iframe>
        </div>
    </div>
</div>
<div class="video-interna-descricao">
    <div class="centro">
        <article>
            <p class="data">01 de agosto de 2014</p>
            <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In feugiat mattis sem, auctor accumsan sapien.</h1>
            <p class="descricao">
                Morbi consectetur diam vitae egestas ullamcorper. Cras consequat eleifend ipsum sed molestie. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
            </p>
            <p class="descricao">
                Suspendisse dapibus scelerisque elit, sed malesuada justo porttitor vel. Ut ac interdum libero. In sed venenatis sed imperdiet auctor, porta lacinia nibh.
            </p>
        </article>
        <aside>
            <h3>Veja também</h3>
            <a href="#" class="veja-thumb">
                <div class="veja-overlay">
                    <img src="assets/img/upload/thumb-120x73.jpg" alt="">
                </div>
                <p>Lorem ipsum dolor sit amet, consec etur adipiscing elit.</p>
            </a>
            <a href="#" class="veja-thumb">
                <div class="veja-overlay">
                    <img src="assets/img/upload/thumb-120x73.jpg" alt="">
                </div>
                <p>Lorem ipsum dolor sit amet, consec etur adipiscing elit.</p>
            </a>
        </aside>
    </div>
</div>