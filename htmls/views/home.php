<div class="home-carousel-show">
    <div class="centro">
        <div class="carousel-video">
            <div class="video-wrapper">
                <iframe src="//player.vimeo.com/video/110566773?api=1&color=f05a8e&portrait=0&autoplay=0&badge=0&title=0&byline=0" width="640" height="355" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
        <div class="carousel-dados">
            <p class="data">01 de agosto de 2014</p>
            <p class="titulo">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <p class="descricao">
                Nulla placerat ligula vel tellus suscipit sagittis. Cras vestibulum sollicitudin ipsum in venenatis. Proin venenatis et turpis ut feugiat. Sed dictum bibendum facilisis. Pellentesque viverra non nisl nec venenatis.
            </p>
            <div class="compartilhe">
                <p>compartilhe:</p>
                <a href="#" class="facebook">facebook</a>
                <a href="#" class="twitter">twitter</a>
                <a href="#" class="pinterest">pinterest</a>
                <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
</div>
<div class="home-carousel-list">
    <div class="centro">
        <div class="carousel">
            <div class="carousel-thumb active" data-videoid="110566773" data-data="01 de agosto de 2014" data-titulo="Lorem ipsum dolor sit amet, consectetur adipiscing elit." data-descricao="Nulla placerat ligula vel tellus suscipit sagittis. Cras vestibulum sollicitudin ipsum in venenatis. Proin venenatis et turpis ut feugiat. Sed dictum bibendum facilisis. Pellentesque viverra non nisl nec venenatis.">
                <a href="#">
                    <img src="assets/img/upload/thumb-190x115.jpg" alt="">
                    <div class="carousel-overlay">
                        <p>Lorem ipsum dolor sit amet, consec etur adipiscing elit.</p>
                    </div>
                </a>
            </div>
            <div class="carousel-thumb" data-videoid="110802045" data-data="05 de setembro de 2014" data-titulo="Teste de título." data-descricao="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, iure. Est cum repudiandae dolore ullam. Deserunt repellat, animi quisquam labore voluptates debitis ipsum impedit distinctio beatae commodi accusamus magni voluptatum.">
                <a href="#">
                    <img src="assets/img/upload/thumb-190x115_2.jpg" alt="">
                    <div class="carousel-overlay">
                        <p>Lorem ipsum dolor sit amet, consec etur adipiscing elit.</p>
                    </div>
                </a>
            </div>
            <div class="carousel-thumb" data-videoid="98891632" data-data="11 de novembro de 2014" data-titulo="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate neque soluta est." data-descricao="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab voluptates molestiae beatae cupiditate ex. Maxime quis, laboriosam nostrum sit. Iste temporibus, eveniet a est placeat.">
                <a href="#">
                    <img src="assets/img/upload/thumb-190x115_3.jpg" alt="">
                    <div class="carousel-overlay">
                        <p>Lorem ipsum dolor sit amet, consec etur adipiscing elit.</p>
                    </div>
                </a>
            </div>
            <div class="carousel-thumb" data-videoid="110566773" data-data="01 de agosto de 2014" data-titulo="Lorem ipsum dolor sit amet, consectetur adipiscing elit." data-descricao="Nulla placerat ligula vel tellus suscipit sagittis. Cras vestibulum sollicitudin ipsum in venenatis. Proin venenatis et turpis ut feugiat. Sed dictum bibendum facilisis. Pellentesque viverra non nisl nec venenatis.">
                <a href="#">
                    <img src="assets/img/upload/thumb-190x115.jpg" alt="">
                    <div class="carousel-overlay">
                        <p>Lorem ipsum dolor sit amet, consec etur adipiscing elit.</p>
                    </div>
                </a>
            </div>
            <div class="carousel-thumb" data-videoid="110802045" data-data="05 de setembro de 2014" data-titulo="Teste de título." data-descricao="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, iure. Est cum repudiandae dolore ullam. Deserunt repellat, animi quisquam labore voluptates debitis ipsum impedit distinctio beatae commodi accusamus magni voluptatum.">
                <a href="#">
                    <img src="assets/img/upload/thumb-190x115_2.jpg" alt="">
                    <div class="carousel-overlay">
                        <p>Lorem ipsum dolor sit amet, consec etur adipiscing elit.</p>
                    </div>
                </a>
            </div>
            <div class="carousel-thumb" data-videoid="98891632" data-data="11 de novembro de 2014" data-titulo="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate neque soluta est." data-descricao="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab voluptates molestiae beatae cupiditate ex. Maxime quis, laboriosam nostrum sit. Iste temporibus, eveniet a est placeat.">
                <a href="#">
                    <img src="assets/img/upload/thumb-190x115_3.jpg" alt="">
                    <div class="carousel-overlay">
                        <p>Lorem ipsum dolor sit amet, consec etur adipiscing elit.</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="home-social centro">
    <div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-width="392" data-height="185" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>
    <div class="home-newsletter">
        <h3>Cadastre-se e receba nossas novidades por e-mail!</h3>
        <form action="" method="post" id="newsletter-form">
            <input type="text" name="nome" id="nome" placeholder="nome" required>
            <input type="email" name="email" id="email" placeholder="email" required>
            <input type="submit" value>
        </form>
        <div class="newsletter-response">Cadastro efetuado com sucesso!</div>
    </div>
</div>
