-- phpMyAdmin SQL Dump
-- version 4.3.2
-- http://www.phpmyadmin.net
--
-- Host: 10.113.0.15
-- Generation Time: 23-Fev-2015 às 14:30
-- Versão do servidor: 5.5.35-0+wheezy1-log
-- PHP Version: 5.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `marrymet_site`
--
use `marryme_site`;
-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(10) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_hover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--
CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL,
  `frase_chamada` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_contato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `frase_chamada`, `facebook`, `instagram`, `email_contato`, `created_at`, `updated_at`) VALUES
(1, 'Frase de chamada para área de contato', 'https://www.facebook.com/www.marrymetv.com.br', 'http://www.instagram.com', 'contato@marrymetv.com.br', '0000-00-00 00:00:00', '2015-01-07 15:08:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--
CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--
CREATE TABLE IF NOT EXISTS `empresa` (
  `id` int(10) unsigned NOT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_coluna_1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_coluna_2` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `empresa`
--

INSERT INTO `empresa` (`id`, `video_url`, `video_id`, `texto_coluna_1`, `texto_coluna_2`, `created_at`, `updated_at`) VALUES
(4, 'http://vimeo.com/75902887', '75902887', 'Texto\r\n', 'Texto\r\n', '0000-00-00 00:00:00', '2014-12-16 23:37:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_12_05_120636_create_usuarios_table', 1),
('2014_12_05_122319_create_empresa_table', 1),
('2014_12_05_122337_create_categorias_table', 1),
('2014_12_05_122350_create_videos_table', 1),
('2014_12_05_122411_create_contato_table', 1),
('2014_12_05_122423_create_contatos_recebidos_table', 1),
('2014_12_12_140234_alter_videos_table', 2),
('2014_12_15_195246_create_newsletter_table', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `newsletter`
--
CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'contato@trupe.net', 'trupe', '$2y$10$PGuH4V3P1PGv4MUV37gxDOrKFlhLcwhZcpr92DMzxyK6EG9GzE8mm', 'OmNAW6fxdZshKwLRXj0k3foXnCu7HMcqzUH9vVOAUuX55cBjFGn4JCKTteJp', '0000-00-00 00:00:00', '2014-12-17 22:37:29'),
(5, 'bebeta@bebetaschiavini.com.br', 'bebeta', '$2y$10$rJKeDMOJLIoVgZs8Ss6vI.uQr6y4muOlX6eBcI3LB2bYDrIUZrHiK', 'zQTb5iCllC209dkh0jSBl5DjYwM5nR95wtFGNO1MmGHCcN24dtyDh3x69qqT', '2014-12-17 22:37:23', '2014-12-18 02:57:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `videos`
--
CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(10) unsigned NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `destaque_home` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `olho` text COLLATE utf8_unicode_ci NOT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `videos`
--

INSERT INTO `videos` (`id`, `categoria_id`, `destaque_home`, `titulo`, `slug`, `thumbnail`, `data`, `olho`, `video_url`, `video_id`, `texto`, `created_at`, `updated_at`) VALUES
(8, 0, 1, 'Vídeo teste da home1', 'video-teste-da-home1', '6106749.jpg', '2014-12-17', 'Teste de inserção de vídeo', 'http://vimeo.com/6106749', '6106749', 'Teste de inserção de vídeo no novo sistema do site Marry Me.', '2014-12-17 22:39:08', '2014-12-17 22:41:34'),
(9, 0, 1, 'Vídeo teste da home2', 'video-teste-da-home2', '29137046.jpg', '2014-12-17', 'Lorem ipsum dolor sit amet consectuer adispicing.', 'http://vimeo.com/29137046', '29137046', 'Lorem ipsum dolor sit amet consectuer adispicing.', '2014-12-17 22:43:46', '2014-12-17 22:43:46'),
(10, 0, 1, 'Vídeo teste', 'video-teste', '54620685.jpg', '2014-12-18', 'Lorem ipsum dolor sit amet.', 'http://vimeo.com/54620685', '54620685', 'Lorem ipsum dolor sit amet consectuer adispicing et.', '2014-12-17 22:44:42', '2014-12-17 22:44:42'),
(11, 0, 1, 'Vídeo teste da home3', 'video-teste-da-home3', '24583774.jpg', '2014-12-17', 'Lorem ipsum dolor', 'http://vimeo.com/24583774', '24583774', 'Lorem ipsum dolor sit amet consectuer adispicing et.', '2014-12-17 22:45:15', '2014-12-17 22:45:15'),
(12, 0, 1, 'Vídeo teste da home4', 'video-teste-da-home4', '35802231.jpg', '2014-12-17', 'Sit amet consectuer', 'http://vimeo.com/35802231', '35802231', 'Lorem ipsum dolor sit amet consectuer adispicing.', '2014-12-17 22:46:40', '2014-12-17 22:46:40'),
(13, 0, 1, 'Vídeo teste da home5', 'video-teste-da-home5', '17228582.jpg', '2014-12-17', 'Consectuer adispicing', 'http://vimeo.com/17228582', '17228582', 'Lorem ipsum dolor sit amet consectuer adispicing.', '2014-12-17 22:47:52', '2014-12-17 22:47:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
