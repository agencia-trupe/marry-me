<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnunciosEstatisticasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('anuncios_estatisticas', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('anuncios_id');

			$table->string('acao'); // click | impressao

			$table->string('url');
			$table->string('ip');
			$table->string('user_agent');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('anuncios_estatisticas');
	}

}
