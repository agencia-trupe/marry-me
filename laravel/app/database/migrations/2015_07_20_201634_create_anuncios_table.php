<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnunciosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('anuncios', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('titulo');

			$table->string('tipo_anuncio'); // premium | lateral_grande | lateral_medio

			$table->string('tipo_arquivo'); // SWF | GIF | JPG
			$table->string('arquivo');

			$table->string('link')->nullable(); // Somente para GIF ou JPG
			$table->string('destino_link')->nullable(); // Somente para GIF ou JPG

			$table->date('data_entrada');
			$table->date('data_saida');

			$table->integer('publicar')->nullable(); // 1 ou 0

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('anuncios');
	}

}
