<?php

class ContatosRecebidosSeeder extends Seeder {

    public function run()
    {
        DB::table('contatos_recebidos')->delete();

        $data = array(
            array(
				'nome' => 'Bruno',
                'email' => 'bruno@marryme.com.br',
                'telefone' => '11 5555-5555',
                'mensagem' => 'Compellingly provide access to installed base communities after sticky functionalities. Conveniently predominate high-payoff opportunities through transparent relationships. Appropriately develop cross-platform potentialities whereas team building content.',
            ),
            array(
				'nome' => 'Bruno',
                'email' => 'bruno@marryme.com.br',
                'telefone' => '11 5555-5555',
                'mensagem' => 'Compellingly provide access to installed base communities after sticky functionalities. Conveniently predominate high-payoff opportunities through transparent relationships. Appropriately develop cross-platform potentialities whereas team building content.',
            ),
            array(
				'nome' => 'Bruno',
                'email' => 'bruno@marryme.com.br',
                'telefone' => '11 5555-5555',
                'mensagem' => 'Compellingly provide access to installed base communities after sticky functionalities. Conveniently predominate high-payoff opportunities through transparent relationships. Appropriately develop cross-platform potentialities whereas team building content.',
            ),
            array(
				'nome' => 'Bruno',
                'email' => 'bruno@marryme.com.br',
                'telefone' => '11 5555-5555',
                'mensagem' => 'Compellingly provide access to installed base communities after sticky functionalities. Conveniently predominate high-payoff opportunities through transparent relationships. Appropriately develop cross-platform potentialities whereas team building content.',
            ),
            array(
				'nome' => 'Bruno',
                'email' => 'bruno@marryme.com.br',
                'telefone' => '11 5555-5555',
                'mensagem' => 'Compellingly provide access to installed base communities after sticky functionalities. Conveniently predominate high-payoff opportunities through transparent relationships. Appropriately develop cross-platform potentialities whereas team building content.',
            ),
            array(
				'nome' => 'Bruno',
                'email' => 'bruno@marryme.com.br',
                'telefone' => '11 5555-5555',
                'mensagem' => 'Compellingly provide access to installed base communities after sticky functionalities. Conveniently predominate high-payoff opportunities through transparent relationships. Appropriately develop cross-platform potentialities whereas team building content.',
            ),
            array(
				'nome' => 'Bruno',
                'email' => 'bruno@marryme.com.br',
                'telefone' => '11 5555-5555',
                'mensagem' => 'Compellingly provide access to installed base communities after sticky functionalities. Conveniently predominate high-payoff opportunities through transparent relationships. Appropriately develop cross-platform potentialities whereas team building content.',
            ),
            array(
				'nome' => 'Bruno',
                'email' => 'bruno@marryme.com.br',
                'telefone' => '11 5555-5555',
                'mensagem' => 'Compellingly provide access to installed base communities after sticky functionalities. Conveniently predominate high-payoff opportunities through transparent relationships. Appropriately develop cross-platform potentialities whereas team building content.',
            )
        );

        DB::table('contatos_recebidos')->insert($data);
    }

}