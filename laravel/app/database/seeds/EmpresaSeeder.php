<?php

class EmpresaSeeder extends Seeder {

    public function run()
    {
        DB::table('empresa')->delete();

        $data = array(
            array(
				'texto_coluna_1' => '<p>Texto</p>',
                'texto_coluna_2' => '<p>Texto</p>'
            )
        );

        DB::table('empresa')->insert($data);
    }

}
