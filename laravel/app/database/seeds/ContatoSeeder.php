<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
				'frase_chamada' => 'Frase de chamada para área de contato',
                'email_contato' => 'contato@marryme.com.br'
            )
        );

        DB::table('contato')->insert($data);
    }

}