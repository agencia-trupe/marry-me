<?php

class CalhausSeeder extends Seeder {

    public function run()
    {
        DB::table('calhaus')->delete();

        $data = array(
            array(
                'titulo' => 'Calhau - Anúncio Premium',
                'tipo_anuncio' => 'premium',
                'publicar' => 1,
            ),
             array(
                'titulo' => 'Calhau - Anúncio Lateral (grande)',
                'tipo_anuncio' => 'lateral_grande',
                'publicar' => 1,
            ),
              array(
                'titulo' => 'Calhau - Anúncio Lateral (médio)',
                'tipo_anuncio' => 'lateral_medio',
                'publicar' => 1,
            )
        );

        DB::table('calhaus')->insert($data);
    }

}