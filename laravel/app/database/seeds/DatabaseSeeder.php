<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersSeeder');
		$this->call('EmpresaSeeder');
		$this->call('ContatoSeeder');
		$this->call('ContatosRecebidosSeeder');
		$this->call('CalhausSeeder');
	}

}
