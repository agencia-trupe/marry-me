<?php
class Vimeo {

	public static function getId($video_url)
	{
		$oembed_endpoint = 'http://vimeo.com/api/oembed';

		$json_url = $oembed_endpoint . '.json?url=' . rawurlencode($video_url) . '&width=640';

		$oembed = json_decode(\Vimeo::curl_get_helper($json_url));
		return isset($oembed->video_id) ? $oembed->video_id : 0;
	}

	private static function curl_get_helper($url) {
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		$return = curl_exec($curl);
		curl_close($curl);
		return $return;
	}

	public static function embed($id, $largura = '100%', $altura = '100%')
	{
		$default_ratio = 16/9;

		if($largura != null && $altura === null)
			$altura = (9 * $largura)/16;

		if($largura === null && $altura != null)
			$largura = (16 * $altura)/9;

		return '<iframe src="//player.vimeo.com/video/'.$id.'?color=f05a8e&portrait=0&autoplay=0&badge=0&title=0&byline=0" width="'.$largura.'" height="'.$altura.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
	}

	// @id: id do vídeo
	// @tamanho: 'small', 'medium', 'large'
	public static function thumb($id, $tamanho = 'large') {

		$tamanhos = ['small', 'medium', 'large'];

		if(!in_array($tamanho, $tamanhos))
			$tamanho = 'large';

		$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
		$url_thumb = isset($hash[0]['thumbnail_'.$tamanho]) ? $hash[0]['thumbnail_'.$tamanho] : false;

		if($url_thumb === false)
			return '';

		$filename = $id.'.jpg';

		if(copy($url_thumb, 'assets/img/videos/'.$filename))
			return $filename;
		else
			return '';
	}
}