<?php
class Tools {

    public static function dimensoesFlash($tipo)
    {
        switch ($tipo) {
            case 'premium':
                return " width='970px'  height='90px' ";
                break;
            case 'lateral_grande':
                return " width='250px'  height='250px' ";
                break;
            case 'lateral_medio':
                return " width='250px'  height='200px' ";
                break;
            default:
                return '';
                break;
        }
    }

    public static function textToLink($text)
    {
        $reg_exUrl = '/((^| |\r|\n)www|(^| |\r|\n)http:\/\/).([a-zA-Z.]+)/';

        if(preg_match($reg_exUrl, $text, $url)){
            return preg_replace($reg_exUrl, "<a href='".Tools::prep_url(trim($url[0]))."' target='_blank'>{$url[0]}</a> ", $text);
        }else{
            return $text;
        }
    }

    public static function converteData($data = '')
    {
        if($data != ''){
        	if(strpos($data, '-') !== FALSE){
        		// Formato Americano -> Converter para BR
        		list($ano, $mes, $dia) = explode('-', $data);
        		return $dia.'/'.$mes.'/'.$ano;
        	}elseif(strpos($data, '/') !== FALSE){
        		// Formato BR -> Converter para Americano
        		list($dia, $mes, $ano) = explode('/', $data);
        		return $ano.'-'.$mes.'-'.$dia;
        	}
        }else{
        	return '';
        }
    }

    public static function prep_url($link)
    {
        if ($link == 'http://' OR $link == '') return '';

        $url = parse_url($link);

        if (!$url OR !isset($url['scheme']))
            $link = 'http://'.$link;

        return $link;
    }

    public static function exibeData($data = '')
    {

        $meses = array(
            '01' => 'janeiro',
            '02' => 'fevereiro',
            '03' => 'março',
            '04' => 'abril',
            '05' => 'maio',
            '06' => 'junho',
            '07' => 'julho',
            '08' => 'agosto',
            '09' => 'setembro',
            '10' => 'outubro',
            '11' => 'novembro',
            '12' => 'dezembro'
        );

        if ($data != '') {
            if(strpos($data, '-') !== FALSE){
                // Formato Americano -> Converter para BR
                list($ano, $mes, $dia) = explode('-', $data);
                return $dia.' de '.$meses[$mes].' de '.$ano;
            }elseif(strpos($data, '/') !== FALSE){
                // Formato BR -> Converter para Americano
                list($dia, $mes, $ano) = explode('/', $data);
                return $dia.' '.$meses[$mes].' '.$ano;
            }
        } else {
            return '';
        }

    }

    public static function validarData($data)
    {
        if($data != ''){
            if(strpos($data, '-') !== FALSE){
                // Formato Americano
                list($ano, $mes, $dia) = explode('-', $data);
                $ano_ok = (int) $ano > 0 && (int) $ano < 3000;
                $mes_ok = (int) $mes > 0 && (int) $mes <= 12;
                $dia_ok = (int) $dia > 0 && (int) $dia <= 31;
                return $ano_ok && $mes_ok && $dia_ok;
            }elseif(strpos($data, '/') !== FALSE){
                // Formato BR
                list($dia, $mes, $ano) = explode('/', $data);
                $ano_ok = (int) $ano > 0 && (int) $ano < 3000;
                $mes_ok = (int) $mes > 0 && (int) $mes <= 12;
                $dia_ok = (int) $dia > 0 && (int) $dia <= 31;
                return $ano_ok && $mes_ok && $dia_ok;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static function ip()
    {
        if(isset($_SERVER["REMOTE_ADDR"]))
            return $_SERVER["REMOTE_ADDR"];
        elseif(isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        elseif(isset($_SERVER["HTTP_CLIENT_IP"]))
            return $_SERVER["HTTP_CLIENT_IP"];
    }

    public static function validarCpf($cpf)
    {
        $cpf = str_pad(preg_replace('/[^0-9]/i', '', $cpf), 11, '0', STR_PAD_LEFT);

        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999'){
            return false;
        }
        else{
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }

    public static function viewGMaps($str = '', $width = FALSE, $height = FALSE){

        //$str = stripslashes(htmlspecialchars_decode($str));

        if($width)
            $str = preg_replace("~width='(\d+)'~", 'width="'.$width.'"', $str);
        if($height)
            $str = preg_replace("~height='(\d+)'~", 'height="'.$height.'"', $str);

		// COM o link 'ver mapa ampliado'
        // return $str;
        // SEM o link 'ver mapa ampliado'
        return preg_replace('~<br \/>(.*)~', '', $str);
    }
}
