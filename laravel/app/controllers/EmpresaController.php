<?php

use \Empresa;

class EmpresaController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$empresa = Empresa::first();

		View::share('seoTitle', 'Marry Me - Quem');
		View::share('seoDescription', Str::words(strip_tags($empresa->texto_coluna_1)), 15);

		// Se resolverem colocar uma imagem específica para o compartilhamento
		// da página QUEM, indicar o caminho completo aqui

		// View::share('seoThumb', url('assets/img/videos/'.NOME_IMAGEM));

		$this->layout->content = View::make('frontend.empresa.index')->with(compact('empresa'));
	}

}
