<?php

use \Videos, \Categorias, \Calhaus, \Anuncios;

class VideosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$categorias = Categorias::ordenado()->get();
		$this->layout->content = View::make('frontend.videos.index_com_categorias')->with(compact('categorias'));
	}

	public function todos()
	{
		//$videos = Videos::ordenado()->paginate(8);
		$videos = Videos::ordenado()->paginate(12);

		$anuncios_laterais_g = \Anuncios::filtro('lateral_grande_internas')->publicos()->ativos()->get();
		$anuncios_laterais_m = \Anuncios::filtro('lateral_medio_internas')->publicos()->ativos()->get();

		$calhau_lateral_g = Calhaus::filtro('lateral_grande_internas')->publicos()->get();
		$calhau_lateral_m = Calhaus::filtro('lateral_medio_internas')->publicos()->get();

		$this->layout->content = View::make('frontend.videos.index')->with(compact('videos'))
																	->with(compact('anuncios_laterais_g'))
																    ->with(compact('anuncios_laterais_m'))
																    ->with(compact('calhau_lateral_g', 'calhau_lateral_m'));
	}

	public function categoria($slug_categoria)
	{
		if($slug_categoria == 'todos'){
			$videos = Videos::ordenado()->paginate(12);

			$anuncios_laterais_g = \Anuncios::filtro('lateral_grande_internas')->publicos()->ativos()->get();
			$anuncios_laterais_m = \Anuncios::filtro('lateral_medio_internas')->publicos()->ativos()->get();

			$calhau_lateral_g = Calhaus::filtro('lateral_grande_internas')->publicos()->get();
			$calhau_lateral_m = Calhaus::filtro('lateral_medio_internas')->publicos()->get();

			$this->layout->content = View::make('frontend.videos.index')->with(compact('videos'))
																		->with(compact('anuncios_laterais_g'))
																    	->with(compact('anuncios_laterais_m'))
																    	->with(compact('calhau_lateral_g', 'calhau_lateral_m'));
		}else{
			$categoria = Categorias::with('videos')->slug($slug_categoria)->first();
			$this->layout->content = View::make('frontend.videos.categoria')->with(compact('categoria'));
		}
	}

	public function detalhes($slug_categoria, $slug_video)
	{
		$video = Videos::slug($slug_video)->first();

		if(!$video) App::abort('404');

		if($slug_categoria == 'todos'){

			$relacionados = Videos::ordenado()
			  						->notThis($video->id)
			  						->limit(2)
			  						->get();

		}else{

			$relacionados = Videos::whereHas('categoria', function($q) use($slug_categoria) {
				$q->where('slug', '=', $slug_categoria);
			})->ordenado()
			  ->notThis($video->id)
			  ->limit(2)
			  ->get();

		}

		View::share('seoTitle', 'Marry Me | '.$video->titulo);
		View::share('seoDescription', $video->olho);
		View::share('seoThumb', url('assets/img/videos/'.$video->thumbnail));
		$this->layout->content = View::make('frontend.videos.detalhes')->with(compact('video'))
																	   ->with(compact('relacionados'));
	}

	public function busca()
	{
		$termo = Input::get('termo');
		View::share(compact('termo'));

		$anuncios_laterais_g = \Anuncios::filtro('lateral_grande_internas')->publicos()->ativos()->get();
		$anuncios_laterais_m = \Anuncios::filtro('lateral_medio_internas')->publicos()->ativos()->get();

		$calhau_lateral_g = Calhaus::filtro('lateral_grande_internas')->publicos()->get();
		$calhau_lateral_m = Calhaus::filtro('lateral_medio_internas')->publicos()->get();

		$videos = Videos::busca($termo)->ordenado()->paginate(12);
		$this->layout->content = View::make('frontend.videos.index')->with(compact('videos'))
																	->with(compact('anuncios_laterais_g'))
																    ->with(compact('anuncios_laterais_m'))
																    ->with(compact('calhau_lateral_g', 'calhau_lateral_m'));
	}
}
