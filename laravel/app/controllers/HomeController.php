<?php

use \Videos, \Newsletter, \Anuncios, \Calhaus;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$destaques = Videos::destaques()->ordenado()->get()->toArray();

		if(isset($destaques[0])){
			$destaque_maior = $destaques[0];
		}else{
			$destaque_maior = false;
		}

		$outros_destaques = Videos::orderBy(DB::raw('RAND()'))->take(4)->get();

		$anuncios_premium = Anuncios::filtro('premium')->publicos()->ativos()->take(1)->ordenado()->get();
		$calhau_premium = Calhaus::filtro('premium')->publicos()->get();

		$anuncios_laterais_g = Anuncios::filtro('lateral_grande_home')->publicos()->ativos()->take(1)->ordenado()->get();
		$anuncios_laterais_m = Anuncios::filtro('lateral_medio_home')->publicos()->ativos()->take(1)->ordenado()->get();

		$calhau_lateral_g = Calhaus::filtro('lateral_grande_home')->publicos()->get();
		$calhau_lateral_m = Calhaus::filtro('lateral_medio_home')->publicos()->get();

		$this->layout->content = View::make('frontend.home.index')->with(compact('destaques'))
																  ->with(compact('destaque_maior'))
																  ->with(compact('outros_destaques'))
																  ->with(compact('anuncios_premium'))
																  ->with(compact('anuncios_laterais_g'))
																  ->with(compact('anuncios_laterais_m'))
																  ->with(compact('calhau_premium', 'calhau_lateral_g', 'calhau_lateral_m'));
	}

	public function cadastroNewsletter()
	{
		$nome = Input::get('nome');
		$email = Input::get('email');

		if(sizeof(Newsletter::where('email', '=', $email)->get()) == 0){
			$n = new Newsletter;
			$n->nome = $nome;
			$n->email = $email;
			$n->save();
		}
	}
}
