<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Newsletter;

class NewsletterController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$registros = Newsletter::paginate(15);
		$this->layout->content = View::make('backend.newsletter.index')->with(compact('registros'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::route('painel.newsletter.index');	
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::route('painel.newsletter.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Redirect::route('painel.newsletter.index');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return Redirect::route('painel.newsletter.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Newsletter::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Cadastro removido com sucesso.');

		return Redirect::route('painel.newsletter.index');
	}

}