<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Contato;

class ContatoController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.contato.index')->with('registros', Contato::all())->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.contato.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Contato;

		$object->frase_chamada = Input::get('frase_chamada');
		$object->email_contato = Input::get('email_contato');
		$object->facebook = Input::get('facebook');
		$object->instagram = Input::get('instagram');


		if($this->limiteInsercao && sizeof( Contato::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));


		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Informações de Contato criadas com sucesso.');
			return Redirect::route('painel.contato.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Informações de Contato!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.contato.edit')->with('registro', Contato::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Contato::find($id);

		$object->frase_chamada = Input::get('frase_chamada');
		$object->email_contato = Input::get('email_contato');
		$object->facebook = Input::get('facebook');
		$object->instagram = Input::get('instagram');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Informações de Contato alteradas com sucesso.');
			return Redirect::route('painel.contato.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Informações de Contato!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Contato::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Informações de Contato removidas com sucesso.');

		return Redirect::route('painel.contato.index');
	}

}