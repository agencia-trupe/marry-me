<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, Anuncios;

class AnunciosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	public function __construct(){
		\Validator::extend('exact_image_size', function($attribute, $value, $parameters)
		{
			// $parameters[0] = largura maxima
			// $parameters[1] = altura maxima

			$getFile = getimagesize(Input::file('arquivo')->getRealPath());
			// $getFile[0] largura do arquivo
			// $getFile[1] altura do arquivo

			return ($getFile[0] == $parameters[0]) && ($getFile[1] == $parameters[1]);
		});

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$filtroTipo = Input::get('filtroTipo');

		$registros = Anuncios::filtro($filtroTipo)->ordenado()->paginate(25);

		$this->layout->content = View::make('backend.anuncios.index')->with(compact('registros', 'filtroTipo'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.anuncios.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$tipo_anuncio = Input::get('tipo');
		$tipo_arquivo = Input::get('tipo_arquivo');

		switch ($tipo_anuncio) {
			case 'premium':
				$dim = '940,90';
				$dim_msg = "940px x 90px";
				break;
			case 'lateral_grande_home':
				$dim = '250,250';
				$dim_msg = "250px x 250px";
				break;
			case 'lateral_grande_internas':
				$dim = '250,250';
				$dim_msg = "250px x 250px";
				break;
			case 'lateral_medio_home':
				$dim = '250,200';
				$dim_msg = "250px x 200px";
				break;
			case 'lateral_medio_internas':
				$dim = '250,200';
				$dim_msg = "250px x 200px";
				break;
			default:
				$dim = '';
				$dim_msg = "";
				break;
		}

		switch ($tipo_arquivo) {
			case 'flash':
				$mimes_permitidos = 'swf';
				break;
			case 'gif':
				$mimes_permitidos = 'gif';
				break;
			case 'imagem':
				$mimes_permitidos = 'jpeg,png';
				break;
			default:
				$mimes_permitidos = '';
				break;
		}

		$validation = \Validator::make(
			Input::all(),
			array(
				'titulo'       => 'required',
				'tipo'         => 'required|in:premium,lateral_grande_home,lateral_grande_internas,lateral_medio_home,lateral_medio_internas',
				'tipo_arquivo' => 'required|in:flash,gif,imagem',
				'arquivo'      => "required|mimes:{$mimes_permitidos}|exact_image_size:{$dim}",
				'link'         => 'required_if:tipo_arquivo,gif,imagem',
				'destino_link' => 'required_if:tipo_arquivo,gif,imagem|in:_blank,_self',
				'data_entrada' => 'required|date_format:d/m/Y',
				'data_saida'   => 'required|date_format:d/m/Y'
			),
			array(
				'exact_image_size' => "O arquivo deve ter o tamanho exato de : {$dim_msg}"
			)
		);

		if ($validation->fails()){
			// Coloca os dados informados na sessão para redirecionar de volta
			Session::flash('formulario', Input::except('arquivo'));

			return Redirect::back()->withErrors($validation);
		}

		$object = new Anuncios;
		$object->titulo = Input::get('titulo');
		$object->tipo_anuncio = $tipo_anuncio;
		$object->tipo_arquivo = $tipo_arquivo;
		$object->link = ($tipo_arquivo == 'flash') ? '' : Input::get('link');
		$object->destino_link = ($tipo_arquivo == 'flash') ? '' : Input::get('destino_link');
		$object->data_entrada = Tools::converteData(Input::get('data_entrada'));
		$object->data_saida = Tools::converteData(Input::get('data_saida'));
		$object->publicar = Input::get('publicar');

		// if($object->publicar == 1){
		// 	\DB::update('update anuncios set publicar = 0 where tipo_anuncio = ?', array($tipo_anuncio));
		// }

		$arquivo = Input::file('arquivo');
		$filename = $arquivo->getClientOriginalName();
		$arquivo->move('assets/ads/', $filename);
		$object->arquivo = $filename;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Anúncio criado com sucesso.');
			return Redirect::route('painel.anuncios.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('arquivo'));
			return Redirect::back()->withErrors(array('Erro ao criar Anúncio! '.$e->getMessage()));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.anuncios.edit')->with('registro', Anuncios::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Anuncios::find($id);

		$tipo_anuncio_antigo = $object->tipo_anuncio;
		$tipo_arquivo_antigo = $object->tipo_arquivo;

		$tipo_anuncio = Input::get('tipo');
		$tipo_arquivo = Input::get('tipo_arquivo');

		switch ($tipo_anuncio) {
			case 'premium':
				$dim = '940,90';
				$dim_msg = "940px x 90px";
				break;
			case 'lateral_grande_home':
				$dim = '250,250';
				$dim_msg = "250px x 250px";
				break;
			case 'lateral_grande_internas':
				$dim = '250,250';
				$dim_msg = "250px x 250px";
				break;
			case 'lateral_medio_home':
				$dim = '250,200';
				$dim_msg = "250px x 200px";
				break;
			case 'lateral_medio_internas':
				$dim = '250,200';
				$dim_msg = "250px x 200px";
				break;
			default:
				$dim = '';
				$dim_msg = "";
				break;
		}

		switch ($tipo_arquivo) {
			case 'flash':
				$mimes_permitidos = 'swf';
				break;
			case 'gif':
				$mimes_permitidos = 'gif';
				break;
			case 'imagem':
				$mimes_permitidos = 'jpg,jpeg,png';
				break;
			default:
				$mimes_permitidos = '';
				break;
		}

		if($tipo_anuncio_antigo != $tipo_anuncio || $tipo_arquivo_antigo != $tipo_arquivo){

			$validation = \Validator::make(
				Input::all(),
				array(
					'titulo'       => 'required',
					'tipo'         => 'required|in:premium,lateral_grande_home,lateral_grande_internas,lateral_medio_home,lateral_medio_internas',
					'tipo_arquivo' => 'required|in:flash,gif,imagem',
					'arquivo'      => "sometimes|mimes:{$mimes_permitidos}|exact_image_size:{$dim}",
					'link'         => 'required_if:tipo_arquivo,gif,imagem',
					'destino_link' => 'required_if:tipo_arquivo,gif,imagem|in:_blank,_self',
					'data_entrada' => 'required|date_format:d/m/Y',
					'data_saida'   => 'required|date_format:d/m/Y'
				),
				array(
					'exact_image_size' => "O arquivo deve ter o tamanho exato de : {$dim_msg}"
				)
			);

		}else{

			$validation = \Validator::make(
				Input::all(),
				array(
					'titulo'       => 'required',
					'tipo'         => 'required|in:premium,lateral_grande_home,lateral_grande_internas,lateral_medio_home,lateral_medio_internas',
					'tipo_arquivo' => 'required|in:flash,gif,imagem',
					'link'         => 'required_if:tipo_arquivo,gif,imagem',
					'destino_link' => 'required_if:tipo_arquivo,gif,imagem|in:_blank,_self',
					'data_entrada' => 'required|date_format:d/m/Y',
					'data_saida'   => 'required|date_format:d/m/Y'
				)
			);
		}

		if ($validation->fails()){
			// Coloca os dados informados na sessão para redirecionar de volta
			Session::flash('formulario', Input::except('arquivo'));

			return Redirect::back()->withErrors($validation);
		}

		$object->titulo = Input::get('titulo');
		$object->tipo_anuncio = $tipo_anuncio;
		$object->tipo_arquivo = $tipo_arquivo;
		$object->link = ($tipo_arquivo == 'flash') ? '' : Input::get('link');
		$object->destino_link = ($tipo_arquivo == 'flash') ? '' : Input::get('destino_link');
		$object->data_entrada = Tools::converteData(Input::get('data_entrada'));
		$object->data_saida = Tools::converteData(Input::get('data_saida'));
		$object->publicar = Input::get('publicar');

		// if($object->publicar == 1){
		// 	\DB::update('update anuncios set publicar = 0 where tipo_anuncio = ? AND id != ?', array($tipo_anuncio, $id));
		// }

		if(Input::file('arquivo')){
			$arquivo = Input::file('arquivo');
			$filename = $arquivo->getClientOriginalName();
			$arquivo->move('assets/ads/', $filename);
			$object->arquivo = $filename;
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Anúncio alterado com sucesso.');
			return Redirect::route('painel.anuncios.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Anúncio! '.$e->getMessage()));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Anuncios::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Anúncio removido com sucesso.');

		return Redirect::route('painel.anuncios.index');
	}

}
