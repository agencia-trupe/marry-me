<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Videos, \Categorias, \Vimeo;

class VideosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Input::has('categoria') && Input::get('categoria') != "0")
			$registros = Categorias::slug(Input::get('categoria'))->first()->videos()->paginate(25);
		elseif(Input::has('categoria') && Input::get('categoria') == "0")
			$registros = Videos::semCategoria()->paginate(25);
		else
			$registros = Videos::ordenado()->paginate(25);		

		$categorias = Categorias::ordenado()->get();
		$filtro = Input::has('categoria') ? Input::get('categoria') : "todos";
		$this->layout->content = View::make('backend.videos.index')->with(compact('registros'))
																   ->with(compact('categorias'))
																   ->with(compact('filtro'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$categoria = Input::get('categoria');
		$categorias = Categorias::ordenado()->get();
		$this->layout->content = View::make('backend.videos.form')->with(compact('categorias'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Videos;

		$object->categoria_id = Input::has('categoria_id') && Input::get('categoria_id') > 0 ? Input::get('categoria_id') : 0;
		$object->destaque_home = Input::has('destaque_home') && Input::get('destaque_home') == '1' ? 1 : 0;
		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->link_fornecedor = Tools::prep_url(Input::get('link_fornecedor'));

		if(sizeof(Videos::slug($object->slug)->get()) > 0){
			Session::flash('formulario.categoria_id', Input::get('categoria_id'));
			Session::flash('formulario.destaque_home', Input::get('destaque_home'));
			Session::flash('formulario.titulo', Input::get('titulo'));
			Session::flash('formulario.data', Input::get('data'));
			Session::flash('formulario.olho', Input::get('olho'));
			Session::flash('formulario.video_url', Input::get('video_url'));
			Session::flash('formulario.texto', Input::get('texto'));
			return Redirect::back()->withErrors(array('Erro ao criar Vídeo! Um vídeo com o nome <b>'.$object->titulo.'</b> já existe'));
		}

		$object->data = Tools::converteData(Input::get('data'));
		$object->olho = Input::get('olho');
		$object->video_url = Input::get('video_url');
		$object->video_id = Vimeo::getId($object->video_url);
		$object->texto = Input::get('texto');
		$object->thumbnail = Vimeo::thumb($object->video_id);
		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Vídeo criado com sucesso.');
			return Redirect::route('painel.videos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Vídeo!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$categorias = Categorias::ordenado()->get();
		$this->layout->content = View::make('backend.videos.edit')->with('registro', Videos::find($id))
																  ->with(compact('categorias'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Videos::find($id);

		$object->categoria_id = Input::has('categoria_id') && Input::get('categoria_id') > 0 ? Input::get('categoria_id') : 0;
		$object->destaque_home = Input::has('destaque_home') && Input::get('destaque_home') == '1' ? 1 : 0;
		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->link_fornecedor = Tools::prep_url(Input::get('link_fornecedor'));

		if(sizeof(Videos::slug($object->slug)->notThis($object->id)->get()) > 0){
			return Redirect::back()->withErrors(array('Erro ao criar Vídeo! Um vídeo com o nome <b>'.$object->titulo.'</b> já existe'));
		}

		$object->data = Tools::converteData(Input::get('data'));
		$object->olho = Input::get('olho');
		$object->texto = Input::get('texto');
		
		if($object->video_url != Input::get('video_url')){
			$object->video_url = Input::get('video_url');
			$object->video_id = Vimeo::getId($object->video_url);
			$object->thumbnail = Vimeo::thumb($object->video_id);
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Vídeo alterado com sucesso.');
			return Redirect::route('painel.videos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Vídeo!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Videos::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Vídeo removido com sucesso.');

		return Redirect::route('painel.videos.index');
	}

}