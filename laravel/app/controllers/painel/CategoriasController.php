<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Categorias;

class CategoriasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.categorias.index')->with('registros', Categorias::ordenado()->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.categorias.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Categorias;

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		if(sizeof(Categorias::slug($object->slug)->get()) > 0){
			Session::flash('formulario.titulo', Input::get('titulo'));
			return Redirect::back()->withErrors(array('Erro ao criar Categoria! Uma categoria com o nome <b>'.$object->titulo.'</b> já existe'));
		}
		
		$imagem = Thumb::make('imagem', 100, 100, 'categorias/');
		if($imagem) $object->imagem = $imagem;

		//$imagem_hover = Thumb::make('imagem_hover', 100, 100, 'categorias/');
		//if($imagem) $object->imagem_hover = $imagem_hover;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria criada com sucesso.');
			return Redirect::route('painel.categorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.categorias.edit')->with('registro', Categorias::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Categorias::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		
		if(sizeof(Categorias::slug($object->slug)->notThis($object->id)->get()) > 0){
			Session::flash('formulario.titulo', Input::get('titulo'));
			return Redirect::back()->withErrors(array('Erro ao alterar Categoria! Uma categoria com o nome <b>'.$object->titulo.'</b> já existe'));
		}


		$imagem = Thumb::make('imagem', 100, 100, 'categorias/');
		if($imagem) $object->imagem = $imagem;

		//$imagem_hover = Thumb::make('imagem_hover', 100, 100, 'categorias/');
		//if($imagem) $object->imagem_hover = $imagem_hover;

		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria alterada com sucesso.');
			return Redirect::route('painel.categorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Categorias::find($id);

		$videos = $object->videos()->get();
		foreach ($videos as $key => $value) {
			$value->categoria_id = 0;
			$value->save();
		}

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Categoria removida com sucesso.');

		return Redirect::route('painel.categorias.index');
	}

}