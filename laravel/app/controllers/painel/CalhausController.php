<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, Calhaus;

class CalhausController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	public function __construct(){
		\Validator::extend('exact_image_size', function($attribute, $value, $parameters)
		{
			// $parameters[0] = largura maxima
			// $parameters[1] = altura maxima

			$getFile = getimagesize(Input::file('arquivo')->getRealPath());
			// $getFile[0] largura do arquivo
			// $getFile[1] altura do arquivo

			return ($getFile[0] == $parameters[0]) && ($getFile[1] == $parameters[1]);
		});

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$registros = Calhaus::all();

		$this->layout->content = View::make('backend.calhaus.index')->with(compact('registros'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::to('painel.calhaus.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::to('painel.calhaus.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Redirect::to('painel.calhaus.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.calhaus.edit')->with('registro', Calhaus::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Calhaus::find($id);

		switch ($object->tipo_anuncio) {
			case 'premium':
				$dim = '940,90';
				$dim_msg = "940px x 90px";
				break;
			case 'lateral_grande_home':
				$dim = '250,250';
				$dim_msg = "250px x 250px";
				break;
			case 'lateral_grande_internas':
				$dim = '250,250';
				$dim_msg = "250px x 250px";
				break;
			case 'lateral_medio_home':
				$dim = '250,200';
				$dim_msg = "250px x 200px";
				break;
			case 'lateral_medio_internas':
				$dim = '250,200';
				$dim_msg = "250px x 200px";
				break;
			default:
				$dim = '';
				$dim_msg = "";
				break;
		}

		switch (Input::get('tipo_arquivo')) {
			case 'flash':
				$mimes_permitidos = 'swf';
				break;
			case 'gif':
				$mimes_permitidos = 'gif';
				break;
			case 'imagem':
				$mimes_permitidos = 'jpg,jpeg,png';
				break;
			default:
				$mimes_permitidos = '';
				break;
		}

		if($object->tipo_arquivo != Input::get('tipo_arquivo')){

			$validation = \Validator::make(
				Input::all(),
				array(
					'titulo'       => 'required',
					'tipo_arquivo' => 'required|in:flash,gif,imagem',
					'arquivo'      => "required|mimes:{$mimes_permitidos}|exact_image_size:{$dim}",
					'link'         => 'required_if:tipo_arquivo,gif,imagem',
					'destino_link' => 'required_if:tipo_arquivo,gif,imagem|in:_blank,_self'
				),
				array(
					'exact_image_size' => "O arquivo deve ter o tamanho exato de : {$dim_msg}"
				)
			);

		}else{

			$validation = \Validator::make(
				Input::all(),
				array(
					'titulo'       => 'required',
					'tipo_arquivo' => 'required|in:flash,gif,imagem',
					'link'         => 'required_if:tipo_arquivo,gif,imagem',
					'destino_link' => 'required_if:tipo_arquivo,gif,imagem|in:_blank,_self',
				)
			);
		}

		if ($validation->fails()){
			// Coloca os dados informados na sessão para redirecionar de volta
			Session::flash('formulario', Input::except('arquivo'));

			return Redirect::back()->withErrors($validation);
		}

		$object->titulo = Input::get('titulo');
		$object->tipo_arquivo = Input::get('tipo_arquivo');
		$object->link = (Input::get('tipo_arquivo') == 'flash') ? '' : Input::get('link');
		$object->destino_link = (Input::get('tipo_arquivo') == 'flash') ? '' : Input::get('destino_link');
		$object->publicar = Input::get('publicar');

		if(Input::file('arquivo')){
			$arquivo = Input::file('arquivo');
			$filename = $arquivo->getClientOriginalName();
			$arquivo->move('assets/ads/', $filename);
			$object->arquivo = $filename;
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Calhau alterado com sucesso.');
			return Redirect::route('painel.calhaus.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Calhau! '.$e->getMessage()));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Redirect::to('painel.calhaus.index');
	}

}
