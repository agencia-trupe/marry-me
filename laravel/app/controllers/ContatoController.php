<?php

use \Contato;

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.contato.index');
	}
	
	public function envio()
	{
		$contato = Contato::first();

		$data['nome'] = Input::get('nome');
		$data['email'] = Input::get('email');
		$data['telefone'] = Input::get('telefone');
		$data['mensagem'] = Input::get('mensagem');

		if($data['nome'] && $data['email'] && $data['mensagem'] && isset($contato->email_contato)){
			Mail::send('emails.contato', $data, function($message) use ($data, $contato)
			{
			    $message->to($contato->email_contato, 'Marry Me')
			    		->subject('Contato via site')
			    		->replyTo($data['email'], $data['nome']);

			    if(Config::get('mail.pretend')) Log::info(View::make('emails.contato', $data)->render());
			});
		}
	}
}
