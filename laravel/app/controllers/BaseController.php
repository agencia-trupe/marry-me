<?php

use \Contato;

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			View::share('contato', Contato::first());
			View::share('listaCategorias', Categorias::ordenado()->get());
			$this->layout = View::make($this->layout);
		}
	}

}
