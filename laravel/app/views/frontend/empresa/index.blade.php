@section('conteudo')

<div class="empresa-video">
    <div class="centro">
        <div class="empresa-video-wrapper">
            {{ Vimeo::embed($empresa->video_id, '940', '530') }}
        </div>
    </div>
</div>
<div class="empresa-texto">
    <div class="centro">
        <h2>Quem</h2>
        <p class="left">
            {{ Tools::textToLink(strip_tags($empresa->texto_coluna_1)) }}
        </p>
        <p class="right">
            {{ Tools::textToLink(strip_tags($empresa->texto_coluna_2)) }}
        </p>
    </div>
</div>

<div class="contem-compartilhe">
    <div class="compartilhe">
        <p>compartilhe:</p>
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::url()) }}" target="_blank" id="share-fb" class="facebook">facebook</a>
        <a href="https://twitter.com/share?url={{ urlencode(Request::url()) }}" target="_blank" id="share-tw" class="twitter">twitter</a>
        <a href="https://www.pinterest.com/pin/create/button/?url={{ urlencode(Request::url()) }}&media={{ $seoThumb or url('/assets/img/img-fb.jpg') }}&description={{ Str::words(strip_tags($empresa->texto_coluna_1), 15) }}" target="_blank" id="share-pin" class="pinterest">pinterest</a>
        <div class="fb-wrapper">
            <div class="fb-like"
                data-href="{{ Request::url() }}"
                data-layout="button_count"
                data-action="like"
                data-show-faces="false"
                data-share="false">
            </div>
        </div>
    </div>
</div>

@stop