@section('conteudo')

<div class="contato-frase">
    <div class="centro">
        <div class="valign"><p>{{{ $contato->frase_chamada }}}</p></div>
    </div>
</div>
<div class="contato centro">
    <div class="contato-email">
        <a href="mailto:{{{ $contato->email_contato }}}">{{{ $contato->email_contato }}}</a>
        <div id="fb-root"></div>
        <div class="fb-like-box" data-href="https://www.facebook.com/www.marrymetv.com.br" data-width="285" data-height="185" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>
    </div>
    <div class="contato-form">
        <form action="" method="post" id="contato-form">
            <input type="text" name="nome" id="nome" placeholder="Nome" required>
            <input type="email" name="email" id="email" placeholder="E-mail" required>
            <input type="text" name="telefone" id="telefone" placeholder="Telefone">
            <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
            <input type="submit" value="Enviar mensagem">
            <div class="ajax-loading"></div>
            <div class="contato-response">Mensagem enviada com sucesso!</div>
        </form>
    </div>
</div>

@stop