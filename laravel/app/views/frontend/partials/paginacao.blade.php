@if ($paginator->getLastPage() > 1)
    
    <?php $previousPage = ($paginator->getCurrentPage() > 1) ? $paginator->getCurrentPage() - 1 : 1; ?>  
    @if($paginator->getCurrentPage() > 1)
        <a href="{{ $paginator->getUrl($previousPage) }}" class="anteriores" title="vídeos anteriores">vídeos anteriores</a>
    @else
        <div role="presentation" class="spacer" style="float: left;width: 150px;height:16px;"></div>
    @endif

    <nav class="paginacao">
        @for ($i = 1; $i <= $paginator->getLastPage(); $i++)
            <a href="{{ $paginator->getUrl($i) }}" @if($paginator->getCurrentPage() == $i) class="active" @endif>
                {{ $i }}
            </a>
        @endfor    
    </nav>

    @if($paginator->getCurrentPage() < $paginator->getLastPage())
        <a href="{{ $paginator->getUrl($paginator->getCurrentPage()+1) }}" class="proximos" title="próximos vídeos">próximos vídeos</a>    
    @else
        <div role="presentation" class="spacer" style="float: right;width: 150px;height:16px;"></div>
    @endif
@endif