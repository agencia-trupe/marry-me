<header>
    <div class="centro">
        <a href="home" class="logo"><img src="assets/img/marryme_marca.png" alt="Marry Me"></a>
        <nav>
            <ul>
                <li><a href="quem" @if(str_is('empresa', Route::currentRouteName())) class="active" @endif>Quem</a></li>
                <li><a href="videos" @if(str_is('videos*', Route::currentRouteName())) class="active" @endif>Vídeos</a></li>
                <li><a href="contato" @if(str_is('contato', Route::currentRouteName())) class="active" @endif>Contato</a></li>
            </ul>
        </nav>
        <div class="social">
            @if($contato->facebook)
                <a href="{{ Tools::prep_url($contato->facebook) }}" target="_blank" class="facebook">Facebook</a>
            @endif
            @if($contato->instagram)
                <a href="{{ Tools::prep_url($contato->instagram) }}" target="_blank" class="instagram">Instagram</a>
            @endif
        </div>
        <form action="busca" method="post" class="busca">
            <input type="text" name="termo" placeholder="BUSCAR" value={{ $termo or "" }}>
            <input type="submit" value>
        </form>
    </div>
</header>