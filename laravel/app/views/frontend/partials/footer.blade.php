<footer>
    <div class="centro">
        <div class="left">
            <nav>
                <a href="home">Home</a>
                <a href="quem">Quem</a>
                <a href="videos">Vídeos</a>
                <a href="contato">Contato</a>
            </nav>
            <p class="copyright">&copy;{{ Date('Y') }} Marry Me TV · Todos os direitos reservados</p>
        </div>
        <div class="link-rodape">
            <a href="home" title="Página Inicial"><img src="assets/img/footer-bg.png" alt="Página Inicial"></a>
        </div>
        <div class="right">
            <div class="social">
                @if($contato->instagram)
                    <a href="{{ Tools::prep_url($contato->instagram) }}" target="_blank" class="instagram">Instagram</a>
                @endif
                @if($contato->facebook)
                    <a href="{{ Tools::prep_url($contato->facebook) }}" target="_blank" class="facebook">Facebook</a>
                @endif
            </div>
            @if($contato->email_contato)
                <a href="mailto:{{ $contato->email_contato }}" class="email">{{ $contato->email_contato }}</a>
            @endif
            <p class="copyright">
                <a href="http://www.trupe.net" target="_blank">Criação de Sites: </a>
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </div>
</footer>