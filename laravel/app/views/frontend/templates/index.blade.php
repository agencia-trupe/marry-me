<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2014 Trupe Design" />

	<meta name="keywords" content="" />

    <title>{{ $seoTitle or "Marry Me" }}</title>
    <meta property="og:title" content="{{ $seoTitle or "Marry Me" }}"/>

    <meta name="description" content="{{ $seoDescription or "Marry Me" }}">
    <meta property="og:description" content="{{ $seoDescription or "Marry Me" }}"/>

    <meta property="og:site_name" content="Marry Me"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="{{ $seoThumb or url('/assets/img/img-fb.jpg') }}"/>
    <meta property="og:url" content="{{ Request::url() }}"/>

    <meta property="fb:admins" content="100002297057504">
    <meta property="fb:app_id" content="307718282762756">

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/main.min.css">
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=307718282762756&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	 {{-- @include('frontend.partials.header_com_categorias') --}}
   @include('frontend.partials.header')
	 @yield('conteudo')
	 @include('frontend.partials.footer')

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
    <script src="assets/js/main.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
</body>
</html>