@section('conteudo')

    <div class="home-carousel-show">
        <div class="centro">
            @if(sizeof($anuncios_premium))
                <div class="adz-premium">
                    @foreach ($anuncios_premium as $anuncio)
                        <div class="adz" data-track="{{ $anuncio->id }}">
                            @if($anuncio->tipo_arquivo == 'flash')
                                <object data="assets/ads/{{ $anuncio->arquivo }}" type="application/x-shockwave-flash" width='100%' height='90px'>
                                    <param name="movie" value="assets/ads/{{ $anuncio->arquivo }}" />
                                </object>
                            @else
                                <a href="{{ Tools::prep_url($anuncio->link) }}" target='{{ $anuncio->destino_link }}'>
                                    <img src="assets/ads/{{ $anuncio->arquivo }}">
                                </a>
                            @endif
                        </div>
                    @endforeach
                </div>
            @else
                @if($calhau_premium)
                    <div class="adz-premium calhau">
                        @foreach ($calhau_premium as $anuncio)
                            <div class="adz calhau">
                                @if($anuncio->tipo_arquivo == 'flash')
                                    <object data="assets/ads/{{ $anuncio->arquivo }}" type="application/x-shockwave-flash" width='100%' height='90px'>
                                        <param name="movie" value="assets/ads/{{ $anuncio->arquivo }}" />
                                    </object>
                                @else
                                    <a href="{{ $anuncio->link }}" target='{{ $anuncio->destino_link }}'>
                                        <img src="assets/ads/{{ $anuncio->arquivo }}">
                                    </a>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif
            @endif

            @if($destaque_maior)
                <div class="carousel-video">
                    <div class="video-wrapper">
                        @if(isset($destaque_maior['video_id']))
                            {{ Vimeo::embed($destaque_maior['video_id'], '640', '355') }}
                        @endif
                    </div>
                </div>
                <div class="carousel-dados">
                    <p class="data">{{ Tools::exibeData($destaque_maior['data']) }}</p>
                    <p class="titulo">{{{ $destaque_maior['titulo'] }}}</p>
                    <p class="descricao">
                        {{{ strip_tags($destaque_maior['olho']) }}}
                    </p>
                    <div class="compartilhe">
                        <p>compartilhe:</p>
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url('videos/todos/'.$destaque_maior['slug'])) }}"
                           id="share-fb"
                           data-location="{{ urlencode(url('videos/todos/'.$destaque_maior['slug'])) }}"
                           class="facebook">
                                facebook
                        </a>
                        <a href="https://twitter.com/share?url={{ urlencode(url('videos/todos/'.$destaque_maior['slug'])) }}"
                            id="share-tw"
                            data-location="{{ urlencode(url('videos/todos/'.$destaque_maior['slug'])) }}"
                            data-title="{{ urlencode($destaque_maior['titulo']) }}"
                            class="twitter">
                                twitter
                        </a>
                        <a href="https://www.pinterest.com/pin/create/button/?url={{ urlencode(url('videos/todos/'.$destaque_maior['slug'])) }}&media={{ urlencode(url('assets/img/'.$destaque_maior['thumbnail'])) }}&description={{ $destaque_maior['titulo'] }}"
                            id="share-pin"
                            data-location="{{ urlencode(url('videos/todos/'.$destaque_maior['slug'])) }}"
                            data-title="{{ urlencode($destaque_maior['titulo']) }}"
                            data-image="{{ urlencode(url('assets/img/videos/'.$destaque_maior['thumbnail'])) }}"
                            class="pinterest">
                                pinterest
                        </a>
                        <div class="fb-wrapper">
                            <div class="fb-like"
                                 data-href="{{ url('videos/todos/'.$destaque_maior['slug']) }}"
                                 data-layout="button_count"
                                 data-action="like"
                                 data-show-faces="false"
                                 data-share="false">
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="home-carousel-list">
        <div class="centro">
            <div class="carousel">
                @if(sizeof($destaques))
                    @foreach($destaques as $k => $destaque)
                        <div class="carousel-thumb @if($k == '0') active @endif"
                             data-videoid="{{ $destaque['video_id'] }}"
                             data-data="{{ Tools::exibeData($destaque['data']) }}"
                             data-titulo="{{{ $destaque['titulo'] }}}"
                             data-descricao="{{{ strip_tags($destaque['olho']) }}}"
                             data-location="{{ url('videos/todos/'.$destaque['slug']) }}"
                             data-imagem="{{ urlencode(url('assets/img/videos/'.$destaque['thumbnail'])) }}">
                                <a href="videos/{{{ $destaque['slug'] }}}">
                                    <img src="assets/img/videos/{{ $destaque['thumbnail'] }}" alt="{{{ $destaque['titulo'] }}}">
                                    <div class="carousel-overlay">
                                        <p>{{{ strip_tags($destaque['titulo']) }}}</p>
                                    </div>
                                </a>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="outros-destaques">
        <div class="centro">

            <div class="listaOutrosDestaques">
                <h2>Vídeos em destaque</h2>
                @foreach($outros_destaques as $outro_destaque)
                    <div class="outroDestaque">
                        <a href="videos/{{ $outro_destaque->categoria->slug or 'todos' }}/{{ $outro_destaque->slug }}" title="{{ $outro_destaque->titulo }}">
                            <img src="assets/img/videos/{{ $outro_destaque->thumbnail }}" alt="{{ $outro_destaque->titulo }}">
                            <div class="outroDestaque-overlay">
                                <p>{{ $outro_destaque->titulo }}</p>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

            <div class="adz-laterais-home">
                @if(sizeof($anuncios_laterais_g))
                    <div class="adz-laterais marginb">
                        @foreach ($anuncios_laterais_g as $anuncio)
                            <div class="adz" data-track="{{ $anuncio->id }}">
                                @if($anuncio->tipo_arquivo == 'flash')
                                    <object data="assets/ads/{{ $anuncio->arquivo }}" type="application/x-shockwave-flash">
                                        <param name="movie" value="assets/ads/{{ $anuncio->arquivo }}" />
                                    </object>
                                @else
                                    <a href="{{ Tools::prep_url($anuncio->link) }}" target='{{ $anuncio->destino_link }}'>
                                        <img src="assets/ads/{{ $anuncio->arquivo }}">
                                    </a>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @else
                    @if($calhau_lateral_g)
                        <div class="adz-laterais marginb calhau">
                            @foreach ($calhau_lateral_g as $anuncio)
                                <div class="adz calhau">
                                    @if($anuncio->tipo_arquivo == 'flash')
                                        <object data="assets/ads/{{ $anuncio->arquivo }}" type="application/x-shockwave-flash">
                                            <param name="movie" value="assets/ads/{{ $anuncio->arquivo }}" />
                                        </object>
                                    @else
                                        <a href="{{ $anuncio->link }}" target='{{ $anuncio->destino_link }}'>
                                            <img src="assets/ads/{{ $anuncio->arquivo }}">
                                        </a>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    @endif
                @endif
                @if(sizeof($anuncios_laterais_m))
                    <div class="adz-laterais">
                        @foreach ($anuncios_laterais_m as $anuncio)
                            <div class="adz" data-track="{{ $anuncio->id }}">
                                @if($anuncio->tipo_arquivo == 'flash')
                                    <object data="assets/ads/{{ $anuncio->arquivo }}" type="application/x-shockwave-flash">
                                        <param name="movie" value="assets/ads/{{ $anuncio->arquivo }}" />
                                    </object>
                                @else
                                    <a href="{{ Tools::prep_url($anuncio->link) }}" target='{{ $anuncio->destino_link }}'>
                                        <img src="assets/ads/{{ $anuncio->arquivo }}">
                                    </a>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @else
                    @if($calhau_lateral_m)
                        <div class="adz-laterais marginb calhau">
                            @foreach ($calhau_lateral_m as $anuncio)
                                <div class="adz calhau">
                                    @if($anuncio->tipo_arquivo == 'flash')
                                        <object data="assets/ads/{{ $anuncio->arquivo }}" type="application/x-shockwave-flash">
                                            <param name="movie" value="assets/ads/{{ $anuncio->arquivo }}" />
                                        </object>
                                    @else
                                        <a href="{{ $anuncio->link }}" target='{{ $anuncio->destino_link }}'>
                                            <img src="assets/ads/{{ $anuncio->arquivo }}">
                                        </a>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>

    <div class="home-social centro">
        <div class="home-social-fb">
            <div class="fb-like-box" data-href="https://www.facebook.com/www.marrymetv.com.br" data-width="447" data-height="185" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>
        </div>
        <div class="home-newsletter">
            <h3>Cadastre-se e receba nossas novidades por e-mail!</h3>
            <form action="" method="post" id="newsletter-form">
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="email" required>
                <input type="submit" value>
            </form>
            <div class="newsletter-response">Cadastro efetuado com sucesso!</div>
        </div>
    </div>

@stop
