@section('conteudo')

<div class="video-interna-player">
    <div class="centro">
        <div class="video-interna-wrapper">
        	{{ Vimeo::embed($video->video_id, 940, 530) }}
        </div>
    </div>
</div>
<div class="video-interna-faixa">
    <div class="centro">
        <a href="#" class="luzes">Clique para apagar as luzes!</a>
        <div class="compartilhe">
            <p>compartilhe:</p>
            <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::url()) }}" target="_blank" id="share-fb" class="facebook">facebook</a>
            <a href="https://twitter.com/share?url={{ urlencode(Request::url()) }}" target="_blank" id="share-tw" class="twitter">twitter</a>
            <a href="https://www.pinterest.com/pin/create/button/?url={{ urlencode(Request::url()) }}&media={{ $seoThumb }}&description={{ $video->titulo }}" target="_blank" id="share-pin" class="pinterest">pinterest</a>
            <div class="fb-wrapper">
                <div class="fb-like"
                    data-href="{{ Request::url() }}"
                    data-layout="button_count"
                    data-action="like"
                    data-show-faces="false"
                    data-share="false">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="video-interna-descricao">
    <div class="centro">
        <article>
            <div class="categoria">
                @if(isset($video->categoria->titulo))
                    <img src="assets/img/categorias/{{ $video->categoria->imagem }}" style='width:38px' alt="{{ $video->categoria->titulo }}">
                    <h3>{{ $video->categoria->titulo }}</h3>
                @endif
                <p class="data">{{ Tools::exibeData($video->data) }}</p>
            </div>
            <h1>{{{ $video->titulo }}}</h1>
            <p class="descricao">
                {{{ $video->olho }}}
            </p>
            <p class="descricao">
                {{{ $video->texto }}}
            </p>
            <p class="descricao linkFornecedor">
                <a href="{{ $video->link_fornecedor }}" target="_blank" title="Visitar site do Fornecedor">{{ $video->link_fornecedor }}</a>
            </p>
        </article>
        @if(sizeof($relacionados) > 0)
	        <aside>
	            <h3>Veja também</h3>
	            @foreach($relacionados as $rel)
		            <a href="videos/{{ $rel->categoria->slug or 'todos' }}/{{ $rel->slug }}" class="veja-thumb">
		                <div class="veja-overlay" style="overflow:hidden;">
		                    <img src="assets/img/videos/{{ $rel->thumbnail }}" style="max-height:73px;">
		                </div>
		                <p>{{{ $rel->titulo }}}</p>
		            </a>
	            @endforeach
	        </aside>
        @endif
    </div>
</div>

@stop