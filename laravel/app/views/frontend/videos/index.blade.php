@section('conteudo')

@if(isset($termo))
	<div class="centro">
		<h2>Resultados para o termo '{{{ $termo }}}'</h2>
	</div>
@endif

<div class="videos centro">
	<div class="coluna-esquerda">
		@if(sizeof($videos) > 0)
			@foreach ($videos as $video)
				<a href="videos/{{ $video->categoria->slug or 'todos' }}/{{ $video->slug }}" class="videos-thumb" title="{{ $video->titulo }}">
			        <div class="video-thumb">
			            <img src="assets/img/videos/{{ $video->thumbnail }}" alt="{{ $video->titulo }}" style="max-height:100%;">
			            <div class="overlay"></div>
			        </div>
			        <div class="video-descricao">
			            <p>
			                <span>{{ Tools::exibeData($video->data) }}</span>
			                {{ strip_tags($video->olho) }}
			            </p>
			        </div>
			    </a>
			@endforeach
		@endif
	</div>
	<div class="coluna-direita">
		<div class="adz-laterais-internas">
            @if($anuncios_laterais_g)
                <div class="adz-laterais marginb">
                    @foreach ($anuncios_laterais_g as $anuncio)
                        <div class="adz adz-laterais-g" data-track="{{ $anuncio->id }}">
                            @if($anuncio->tipo_arquivo == 'flash')
                                <object data="assets/ads/{{ $anuncio->arquivo }}" type="application/x-shockwave-flash">
                                    <param name="movie" value="assets/ads/{{ $anuncio->arquivo }}" />
                                </object>
                            @else
                                <a href="{{ Tools::prep_url($anuncio->link) }}" target='{{ $anuncio->destino_link }}'>
                                    <img src="assets/ads/{{ $anuncio->arquivo }}">
                                </a>
                            @endif
                        </div>
                    @endforeach
                </div>
            @else
                @if($calhau_lateral_g)
                    <div class="adz-laterais marginb calhau">
                        @foreach ($calhau_lateral_g as $anuncio)
                            <div class="adz adz-laterais-g" data-track="{{ $anuncio->id }}">
                                @if($anuncio->tipo_arquivo == 'flash')
                                    <object data="assets/ads/{{ $anuncio->arquivo }}" type="application/x-shockwave-flash">
                                        <param name="movie" value="assets/ads/{{ $anuncio->arquivo }}" />
                                    </object>
                                @else
                                    <a href="{{ $anuncio->link }}" target='{{ $anuncio->destino_link }}'>
                                        <img src="assets/ads/{{ $anuncio->arquivo }}">
                                    </a>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif
            @endif
            @if($anuncios_laterais_m)
                <div class="adz-laterais">
                    @foreach ($anuncios_laterais_m as $anuncio)
                        <div class="adz adz-laterais-m " data-track="{{ $anuncio->id }}">
                            @if($anuncio->tipo_arquivo == 'flash')
                                <object data="assets/ads/{{ $anuncio->arquivo }}" type="application/x-shockwave-flash">
                                    <param name="movie" value="assets/ads/{{ $anuncio->arquivo }}" />
                                </object>
                            @else
                                <a href="{{ Tools::prep_url($anuncio->link) }}" target='{{ $anuncio->destino_link }}'>
                                    <img src="assets/ads/{{ $anuncio->arquivo }}">
                                </a>
                            @endif
                        </div>
                    @endforeach
                </div>
            @else
                @if($calhau_lateral_m)
                    <div class="adz-laterais marginb calhau">
                        @foreach ($calhau_lateral_m as $anuncio)
                            <div class="adz adz-laterais-m " data-track="{{ $anuncio->id }}">
                                @if($anuncio->tipo_arquivo == 'flash')
                                    <object data="assets/ads/{{ $anuncio->arquivo }}" type="application/x-shockwave-flash">
                                        <param name="movie" value="assets/ads/{{ $anuncio->arquivo }}" />
                                    </object>
                                @else
                                    <a href="{{ $anuncio->link }}" target='{{ $anuncio->destino_link }}'>
                                        <img src="assets/ads/{{ $anuncio->arquivo }}">
                                    </a>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif
            @endif
        </div>
	</div>
</div>

<div class="videos-paginacao">
    <div class="centro">
    	@if(isset($termo))
    		{{ $videos->appends(compact('termo'))->links() }}
		@else
			{{ $videos->links() }}
		@endif
    </div>
</div>

@stop