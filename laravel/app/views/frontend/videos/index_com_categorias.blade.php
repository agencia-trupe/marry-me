@section('conteudo')

<div class="categorias-lista">
	@if(sizeof($categorias) > 0)
		@foreach ($categorias as $categoria)
		    <a href="videos/{{ $categoria->slug }}">
		        <img src="assets/img/categorias/{{ $categoria->imagem }}" alt="{{ $categoria->titulo }}">
		        <p>{{ $categoria->titulo }}</p>
		    </a>
    	@endforeach
	@endif
</div>

@stop