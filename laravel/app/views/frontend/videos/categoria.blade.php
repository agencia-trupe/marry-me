@section('conteudo')

<div class="categoria-titulo">
    <div class="centro">
        <img src="assets/img/categorias/{{ $categoria->imagem }}" style="width:38px;" alt="{{ $categoria->titulo }}">
        <h2>{{ $categoria->titulo }}</h2>
    </div>
</div>

<div class="centro">
    <div class="videos-categorias">
	    @if(sizeof($categoria->videos))
	    	@foreach ($categoria->videos as $video)
		        <a href="videos/{{ $categoria->slug }}/{{ $video->slug }}" class="videos-thumb" title="{{ $video->titulo }}">
		            <div class="video-thumb">
		                <img src="assets/img/videos/{{ $video->thumbnail }}" alt="{{ $video->titulo }}" style="max-height:100%;">
		                <div class="overlay"></div>
		            </div>
		            <div class="video-descricao">
		                <p>
		                    <span>{{ Tools::exibeData($video->data) }}</span>
		                	{{ strip_tags($video->olho) }}
		                </p>
		            </div>
		        </a>	    		
	    	@endforeach
	    @endif
    </div>
    <aside class="videos-categorias-aside">
        <div class="categorias">
            <h3>Categorias</h3>
            @if(sizeof($listaCategorias))
            	@foreach ($listaCategorias as $cat)
            		<a href="videos/{{ $cat->slug }}" @if($cat->slug == $categoria->slug) class='active' @endif>{{ $cat->titulo }}</a>
            	@endforeach
            @endif
        </div>
    </aside>
</div>

@stop