<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title>Marry Me - Painel Administrativo</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/jquery-ui/themes/base/jquery-ui',
		'vendor/bootstrap/dist/css/bootstrap.min',
		'css/painel/painel'
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('vendor/modernizr/modernizr'))?>
	@endif

</head>
	<body @if(Route::currentRouteName() == 'painel.login') class="body-painel-login" @else class="body-painel" @endif >

		@if(Route::currentRouteName() != 'painel.login')

			<nav class="navbar navbar-default">
				<div class="navbar-inner">
					<a href="{{URL::route('painel.home')}}" title="Página Inicial" class="navbar-brand">Marry Me</a>

					<ul class="nav navbar-nav">

						<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.home')}}" title="Início">Início</a>
						</li>

						<li class="dropdown @if(preg_match('~painel.(anuncios*|calhaus*)~', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Anúncios <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.anuncios.index')}}" title="Categorias">Anúncios</a></li>
								<li><a href="{{URL::route('painel.calhaus.index')}}" title="Vídeos">Calhaus</a></li>
							</ul>
						</li>

						<li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.empresa.index')}}" title="Empresa">Empresa</a>
						</li>

						<li class="dropdown @if(preg_match('~painel.(videos*|categorias*)~', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Vídeos <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.categorias.index')}}" disabled="disabled" title="Categorias">Categorias</a></li>
								<li><a href="{{URL::route('painel.videos.index')}}" title="Vídeos">Vídeos</a></li>
							</ul>
						</li>

						<li class="dropdown @if(preg_match('~painel.(contato*|newsletter*)~', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Contato <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.contato.index')}}" title="Informações de Contato">Informações de Contato</a></li>
								<li><a href="{{URL::route('painel.contatosrecebidos.index')}}" title="Contatos Recebidos">Contatos Recebidos</a></li>
								<li role="presentation" class="divider"></li>
								<li><a href="{{URL::route('painel.newsletter.index')}}" title="Cadastros na Newsletter">Cadastros na Newsletter</a></li>
							</ul>
						</li>

						<li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários</a></li>
								<li role="presentation" class="divider"></li>
								<li><a href="{{URL::route('painel.off')}}" title="Logout">Logout</a></li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>

		@endif

		<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>

		<footer>
			<div class="container">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		@if(Route::currentRouteName() == 'painel.login')
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/login'
			))?>
		@else
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/painel'
			))?>
		@endif

	</body>
</html>
