@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Anúncio
        </h2>

		{{ Form::open( array('route' => array('painel.anuncios.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>

				<div class="form-group">
					<label for="inputTipo">Tipo</label>
					<select	name="tipo" class="form-control" id="inputTipo" required>
						<option value="">Selecione</option>
						<option value="premium" data-dimensions="940px x 90px" @if($registro->tipo_anuncio == 'premium') selected @endif >Anúncio Premium - 940px x 90px</option>
						<option value="lateral_grande_home" data-dimensions="250px x 250px" @if($registro->tipo_anuncio == 'lateral_grande_home') selected @endif >Anúncio Lateral para Home (grande) - 250px x 250px</option>
              <option value="lateral_medio_home" data-dimensions="250px x 200px" @if($registro->tipo_anuncio == 'lateral_medio_home') selected @endif >Anúncio Lateral para Home (médio) - 250px x 200px</option>
                <option value="lateral_grande_internas" data-dimensions="250px x 250px" @if($registro->tipo_anuncio == 'lateral_grande_internas') selected @endif >Anúncio Lateral para Internas (grande) - 250px x 250px</option>
              <option value="lateral_medio_internas" data-dimensions="250px x 200px" @if($registro->tipo_anuncio == 'lateral_medio_internas') selected @endif >Anúncio Lateral para Internas (médio) - 250px x 200px</option>
					</select>
				</div>

				<div class="form-group">
					<label for="inputTipoArquivo">Tipo de Arquivo</label>
					<select	name="tipo_arquivo" class="form-control" id="inputTipoArquivo" required>
						<option value="">Selecione</option>
						<option value="flash" @if($registro->tipo_arquivo == 'flash') selected @endif >Flash (SWF)</option>
						<option value="gif" @if($registro->tipo_arquivo == 'gif') selected @endif >GIF</option>
						<option value="imagem" @if($registro->tipo_arquivo == 'imagem') selected @endif >Imagem (JPG ou PNG)</option>
					</select>
				</div>
			</div>

				<div class="well">
					<label>Arquivo Atual</label><br>
					@if($registro->tipo_arquivo == 'flash')
						<object data="assets/ads/{{ $registro->arquivo }}" type="application/x-shockwave-flash" {{ Tools::dimensoesFlash($registro->tipo_anuncio) }}>
							<param name="movie" value="assets/ads/{{ $registro->arquivo }}" />
						</object
					@else
						<img src="assets/ads/{{ $registro->arquivo }}">
					@endif
				</div>

			<div class="pad">

				<div class="form-group" id="hasInputArquivo">
					<label for="inputArquivo">Novo Arquivo <span id="file_dimensions_tip"></span> </label>
					<input type="file" class="form-control" id="inputArquivo" name="arquivo">
				</div>

				<div id="hasInputLinks" style="display:none;">

					<div class="form-group">
						<label for="inputLink">Link</label>
						<input type="text" class="form-control" id="inputLink" name="link" value="{{ $registro->link }}">
					</div>

					<div class="form-group">
						<label for="inputDestino do Link">Destino do Link</label>
						<select	name="destino_link" class="form-control" id="inputDestino do Link" >
							<option value="">Selecione</option>
							<option value="_blank" @if($registro->destino_link == '_blank') selected @endif >Nova Aba</option>
							<option value="_self" @if($registro->destino_link == '_self') selected @endif >Mesma Aba</option>
						</select>
					</div>
				</div>

				<hr>

				<div class="well">

					<label>Período de Exibição</label>

					<hr>

					<div class="form-group">
						<label for="inputData de Entrada">Data de Início</label>
						<input type="text" class="form-control datepicker" id="inputData de Entrada" name="data_entrada" value="{{ Tools::converteData($registro->data_entrada) }}" required>
					</div>

					<div class="form-group">
						<label for="inputData de Saída">Data de Saída</label>
						<input type="text" class="form-control datepicker" id="inputData de Saída" name="data_saida" value="{{ Tools::converteData($registro->data_saida) }}" required>
					</div>

				</div>

				<hr>

				<div class="well">
					<div class="form-group">
						<label><input type="checkbox" name="publicar" value="1" @if($registro->publicar == 1) checked @endif > O anúncio está pronto para ser Publicado</label>
					</div>
				</div>

				<hr>


				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.anuncios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>

@stop
