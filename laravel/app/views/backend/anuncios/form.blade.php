@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Anúncio
        </h2>

		<form action="{{URL::route('painel.anuncios.store')}}" method="post" enctype="multipart/form-data" novalidate>
			<div class="pad">

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputTipo">Tipo</label>
					<select	name="tipo" class="form-control" id="inputTipo" required>
						<option value="">Selecione</option>
						<option value="premium" data-dimensions="940px x 90px" @if(Session::has('formulario') && Session::get('formulario.tipo') == 'premium') selected @endif >Anúncio Premium - 940px x 90px</option>
						<option value="lateral_grande_home" data-dimensions="250px x 250px" @if(Session::has('formulario') && Session::get('formulario.tipo') == 'lateral_grande_home') selected @endif >Anúncio Lateral para Home (grande) - 250px x 250px</option>
            <option value="lateral_medio_home" data-dimensions="250px x 200px" @if(Session::has('formulario') && Session::get('formulario.tipo') == 'lateral_medio_home') selected @endif >Anúncio Lateral para Home (médio) - 250px x 200px</option>
            <option value="lateral_grande_internas" data-dimensions="250px x 250px" @if(Session::has('formulario') && Session::get('formulario.tipo') == 'lateral_grande_internas') selected @endif >Anúncio Lateral para Internas(grande) - 250px x 250px</option>
              <option value="lateral_medio_internas" data-dimensions="250px x 200px" @if(Session::has('formulario') && Session::get('formulario.tipo') == 'lateral_medio_internas') selected @endif >Anúncio Lateral para Internas(médio) - 250px x 200px</option>
					</select>
				</div>

				<div class="form-group">
					<label for="inputTipoArquivo">Tipo de Arquivo</label>
					<select	name="tipo_arquivo" class="form-control" id="inputTipoArquivo" required>
						<option value="">Selecione</option>
						<option value="flash" @if(Session::has('formulario') && Session::get('formulario.tipo_arquivo') == 'flash') selected @endif >Flash (SWF)</option>
						<option value="gif" @if(Session::has('formulario') && Session::get('formulario.tipo_arquivo') == 'gif') selected @endif >GIF</option>
						<option value="imagem" @if(Session::has('formulario') && Session::get('formulario.tipo_arquivo') == 'imagem') selected @endif >Imagem (JPG ou PNG)</option>
					</select>
				</div>

				<div class="form-group" id="hasInputArquivo" style="display:none;">
					<label for="inputArquivo">Arquivo <span id="file_dimensions_tip"></span> </label>
					<input type="file" class="form-control" id="inputArquivo" name="arquivo" required>
				</div>

				<div id="hasInputLinks" style="display:none;">

					<div class="form-group">
						<label for="inputLink">Link</label>
						<input type="text" class="form-control" id="inputLink" name="link" @if(Session::has('formulario')) value="{{ Session::get('formulario.link') }}" @endif >
					</div>

					<div class="form-group">
						<label for="inputDestino do Link">Destino do Link</label>
						<select	name="destino_link" class="form-control" id="inputDestino do Link" >
							<option value="">Selecione</option>
							<option value="_blank" @if(Session::has('formulario') && Session::get('formulario.destino_link') == '_blank') selected @endif >Nova Aba</option>
							<option value="_self" @if(Session::has('formulario') && Session::get('formulario.destino_link') == '_self') selected @endif >Mesma Aba</option>
						</select>
					</div>
				</div>

				<hr>

				<div class="well">

					<label>Período de Exibição</label>

					<hr>

					<div class="form-group">
						<label for="inputData de Entrada">Data de Início</label>
						<input type="text" class="form-control datepicker" id="inputData de Entrada" name="data_entrada" @if(Session::has('formulario')) value="{{ Session::get('formulario.data_entrada') }}" @endif required>
					</div>

					<div class="form-group">
						<label for="inputData de Saída">Data de Saída</label>
						<input type="text" class="form-control datepicker" id="inputData de Saída" name="data_saida" @if(Session::has('formulario')) value="{{ Session::get('formulario.data_saida') }}" @endif required>
					</div>

				</div>

				<hr>

				<div class="well">
					<div class="form-group">
						<label><input type="checkbox" name="publicar" value="1"> O anúncio está pronto para ser Publicado</label>
					</div>
				</div>

				<hr>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.anuncios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>

@stop
