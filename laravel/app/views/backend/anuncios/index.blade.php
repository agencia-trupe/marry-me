@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Anúncios <a href='{{ URL::route('painel.anuncios.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Anúncio</a>
    </h2>

    <hr>

    <div class="btn-group">
        <a href="{{ URL::route('painel.anuncios.index', array('filtroTipo' => 'premium')) }}" title="" class="btn btn-sm btn-info @if($filtroTipo == 'premium') btn-warning @endif ">Premium</a>
        <a href="{{ URL::route('painel.anuncios.index', array('filtroTipo' => 'lateral_grande_home')) }}" title="" class="btn btn-sm btn-info @if($filtroTipo == 'lateral_grande_home') btn-warning @endif ">Lateral para Home (grande)</a>
        <a href="{{ URL::route('painel.anuncios.index', array('filtroTipo' => 'lateral_medio_home')) }}" title="" class="btn btn-sm btn-info @if($filtroTipo == 'lateral_medio_home') btn-warning @endif ">Lateral para Home (médio)</a>
        <a href="{{ URL::route('painel.anuncios.index', array('filtroTipo' => 'lateral_grande_internas')) }}" title="" class="btn btn-sm btn-info @if($filtroTipo == 'lateral_grande_internas') btn-warning @endif ">Lateral para Internas (grande)</a>
        <a href="{{ URL::route('painel.anuncios.index', array('filtroTipo' => 'lateral_medio_internas')) }}" title="" class="btn btn-sm btn-info @if($filtroTipo == 'lateral_medio_internas') btn-warning @endif ">Lateral para Internas (médio)</a>
    </div>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
				<th rowspan="2">Publicado</th>
                <th rowspan="2">Título</th>
                <th rowspan="2">Período de Exibição</th>
                <th colspan="3" style='text-align:center;'>Estatísticas</th>
                <th rowspan="2"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
            <tr>
                <th>Clicks</th>
                <th>Impressões</th>
                <th>CTR <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="A CTR (click-through rate) ou Taxa de Cliques é o número de cliques recebidos pelo anúncio dividido pelo número de vezes que ele é exibido (impressões)"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
				<td>
                    @if($registro->publicar == 1)
                        <span class="glyphicon glyphicon-ok"></span>
                    @else
                        <span class="glyphicon glyphicon-remove"></span>
                    @endif
                </td>
                <td>{{ $registro->titulo }}</td>
                <td>
                    Início: {{ Tools::converteData($registro->data_entrada) }}
                    <br>
                    Término: {{ Tools::converteData($registro->data_saida) }}
                </td>
                <td>
                    {{ sizeof($registro->clicks) }}
                </td>
                <td>
                    {{ sizeof($registro->impressoes) }}
                </td>
                <td>
                    {{ $registro->ctr() }}
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.anuncios.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
          					{{ Form::open(array('route' => array('painel.anuncios.destroy', $registro->id), 'method' => 'delete')) }}
          						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
          					{{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop
