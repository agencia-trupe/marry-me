@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Calhau
        </h2>

		{{ Form::open( array('route' => array('painel.calhaus.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>

				<div class="form-group">
					<label for="inputTipoArquivo">Tipo de Arquivo</label>
					<select	name="tipo_arquivo" class="form-control" id="inputTipoArquivo" required>
						<option value="">Selecione</option>
						<option value="flash" @if($registro->tipo_arquivo == 'flash') selected @endif >Flash (SWF)</option>
						<option value="gif" @if($registro->tipo_arquivo == 'gif') selected @endif >GIF</option>
						<option value="imagem" @if($registro->tipo_arquivo == 'imagem') selected @endif >Imagem (JPG ou PNG)</option>
					</select>
				</div>
			</div>

				<div class="well">
					<label>Arquivo Atual</label><br>
					@if($registro->tipo_arquivo == 'flash')
						<object data="assets/ads/{{ $registro->arquivo }}" type="application/x-shockwave-flash" {{ Tools::dimensoesFlash($registro->tipo_anuncio) }}>
							<param name="movie" value="assets/ads/{{ $registro->arquivo }}" />
						</object
					@else
						<img src="assets/ads/{{ $registro->arquivo }}">
					@endif
				</div>

			<div class="pad">

				<div class="form-group" id="hasInputArquivo">
					<label for="inputArquivo">Novo Arquivo - <span id="file_dimensions_tip"> @if($registro->tipo_anuncio == 'premium') (940px x 90px) @elseif(str_is('lateral_grande_*', $registro->tipo_anuncio)) 250px x 250px @else 250px x 200px @endif </span> </label>
					<input type="file" class="form-control" id="inputArquivo" name="arquivo">
				</div>

				<div id="hasInputLinks" style="display:none;">

					<div class="form-group">
						<label for="inputLink">Link</label>
						<input type="text" class="form-control" id="inputLink" name="link" value="{{ $registro->link }}">
					</div>

					<div class="form-group">
						<label for="inputDestino do Link">Destino do Link</label>
						<select	name="destino_link" class="form-control" id="inputDestino do Link" >
							<option value="">Selecione</option>
							<option value="_blank" @if($registro->destino_link == '_blank') selected @endif >Nova Aba</option>
							<option value="_self" @if($registro->destino_link == '_self') selected @endif >Mesma Aba</option>
						</select>
					</div>
				</div>

				<hr>

				<div class="well">
					<div class="form-group">
						<label><input type="checkbox" name="publicar" value="1" @if($registro->publicar == 1) checked @endif > O calhau pode ser exibido caso não haja anúncios ativos deste Tipo</label>
					</div>
				</div>

				<hr>


				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.calhaus.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>

@stop
