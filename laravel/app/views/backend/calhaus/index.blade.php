@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Calhaus
    </h2>

    <div class="pad">
        <div class="alert alert-block alert-info">
            Os <strong>calhaus</strong> são substitutos dos anúncios. Eles são exibidos caso não haja nenhum anúncio disponível para exibição, para cobrir a lacuna do espaço publicitário.
        </div>
    </div>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
				<th>Publicado</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
				<td>
                    @if($registro->publicar == 1)
                        <span class="glyphicon glyphicon-ok"></span>
                    @else
                        <span class="glyphicon glyphicon-remove"></span>
                    @endif
                </td>
                <td>{{ $registro->titulo }}</td>

                <td class="crud-actions">
                    <a href='{{ URL::route('painel.calhaus.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>
</div>

@stop