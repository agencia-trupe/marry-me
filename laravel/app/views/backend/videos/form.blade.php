@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Vídeo
        </h2>  

		<form action="{{URL::route('painel.videos.store')}}" method="post" enctype="multipart/form-data" id="form-video">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputCategoria do Vídeo">Categoria do Vídeo</label>					 
					<select	name="categoria_id" class="form-control" id="inputCategoria do Vídeo" >
						<option value="0">Sem Categoria</option>
						@if(sizeof($categorias))
							@foreach ($categorias as $categoria)
								<option value="{{ $categoria->id }}" @if(Session::has('formulario') && Session::get('formulario.categoria_id') == $categoria->id) selected @endif >{{ $categoria->titulo }}</option>
							@endforeach
						@endif
					</select>
				</div>
				
				<div class="well">
					<div class="checkbox">
						<label><input type="checkbox" name="destaque_home" value="1" @if(Session::has('formulario') && Session::get('formulario.destaque_home') == '1') checked @endif> Destaque na Home</label>
					</div>
				</div>

				<div class="well">
					<div class="form-group" id="group-url-vimeo">
						<label for="inputURLdoVídeo">URL do Video (Vimeo)</label>
						<input type="text" class="form-control" id="inputURLdoVídeo" name="video_url" @if(Session::has('formulario')) value="{{ Session::get('formulario.video_url') }}" @endif required>
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputData">Data</label>
					<input type="text" class="form-control datepicker" id="inputData" name="data" @if(Session::has('formulario')) value="{{ Session::get('formulario.data') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputOlho">Olho</label>
					<textarea name="olho" class="form-control" id="inputOlho" >@if(Session::has('formulario')) {{ Session::get('formulario.olho') }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >@if(Session::has('formulario')) {{ Session::get('formulario.texto') }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputLinkFornecedor">Link para o site do Fornecedor</label>
					<input type="text" placeholder="http://" class="form-control" id="inputLinkFornecedor" name="link_fornecedor" @if(Session::has('formulario')) value="{{ Session::get('formulario.link_fornecedor') }}" @endif>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.videos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop