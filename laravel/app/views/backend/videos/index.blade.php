@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Vídeos <a href='{{ URL::route('painel.videos.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Vídeo</a>
    </h2>

    @if(sizeof($categorias))
        <div class="btn-group">
            <a href="{{ URL::route('painel.videos.index') }}" class="btn btn-sm btn-default @if($filtro == 'todos') btn-warning @endif">Todos os Vídeos</a>
            <a href="{{ URL::route('painel.videos.index', array('categoria' => '0')) }}" class="btn btn-sm btn-default @if('0' == $filtro) btn-warning @endif">Vídeos sem Categoria</a>
        @foreach($categorias as $categoria)
            <a href="{{ URL::route('painel.videos.index', array('categoria' => $categoria->slug)) }}" class="btn btn-sm btn-default @if($categoria->slug == $filtro) btn-warning @endif">{{ $categoria->titulo }}</a>
        @endforeach
        </div>
    @endif

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Thumb</th>
                <th>Título</th>
                <th>Data</th>
                <th>Olho</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td><img src="assets/img/videos/{{ $registro->thumbnail }}" style="max-width:150px;"></td>
                <td style="white-space:nowrap">{{ $registro->titulo }}</td>
                <td style="white-space:nowrap">{{ Tools::converteData($registro->data) }}</td>
                <td>{{ Str::words(strip_tags($registro->olho), 15) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.videos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
						{{ Form::open(array('route' => array('painel.videos.destroy', $registro->id), 'method' => 'delete')) }}
							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
						{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop