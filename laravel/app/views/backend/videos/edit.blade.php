@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Vídeo
        </h2>  

		{{ Form::open( array('route' => array('painel.videos.update', $registro->id), 'files' => true, 'method' => 'put', 'id' => 'form-video') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputCategoria do Vídeo">Categoria do Vídeo</label>					 
					<select	name="categoria_id" class="form-control" id="inputCategoria do Vídeo" >
						<option value="0" @if($registro->categoria_id === "0") selected @endif >Sem Categoria</option>
						@if(sizeof($categorias))
							@foreach ($categorias as $categoria)
								<option value="{{ $categoria->id }}" @if($registro->categoria_id == $categoria->id) selected @endif >{{ $categoria->titulo }}</option>
							@endforeach
						@endif
					</select>
				</div>
				
				<div class="well">
					<div class="checkbox">
						<label><input type="checkbox" name="destaque_home" value="1" @if($registro->destaque_home == '1') checked @endif> Destaque na Home</label>
					</div>
				</div>

				<div class="well">
					<div class="form-group" id="group-url-vimeo">
						<label for="inputURLdoVídeoEdit">URL do Vídeo (Vimeo)</label>
						<input type="text" class="form-control" id="inputURLdoVídeoEdit" name="video_url" value="{{$registro->video_url}}" required><br>
						<hr>						
						{{ Vimeo::embed($registro->video_id, '560px', null) }}
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputData">Data</label>
					<input type="text" class="form-control datepicker" id="inputData" name="data" value="{{ Tools::converteData($registro->data) }}" required>
				</div>
				
				<div class="form-group">
					<label for="inputOlho">Olho</label>
					<textarea name="olho" class="form-control" id="inputOlho" >{{$registro->olho }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputLinkFornecedor">Link para o site do Fornecedor</label>
					<input type="text" placeholder="http://" class="form-control" id="inputLinkFornecedor" name="link_fornecedor" value="{{$registro->link_fornecedor}}">
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.videos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop