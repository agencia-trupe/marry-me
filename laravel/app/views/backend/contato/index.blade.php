@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Informações de Contato 
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Frase da Seção</th>
				<th>Email de Contato</th>
				<th>Facebook</th>
                <th>Instagram</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->frase_chamada }}</td>
				<td>{{ $registro->email_contato }}</td>
				<td>
                    @if($registro->facebook)
                        <a href="{{ Tools::prep_url($registro->facebook) }}" title="Facebook" class="btn btn-sm btn-info" target="_blank">facebook</a> 
                    @else
                        <button title="Não cadastrado" class="btn btn-sm btn-default" disabled="disabled">facebook</button> 
                    @endif
                </td>
                <td>
                    @if($registro->instagram)
                        <a href="{{ Tools::prep_url($registro->instagram) }}" title="Instagram" class="btn btn-sm btn-info" target="_blank">instagram</a> 
                    @else
                        <button title="Não cadastrado" class="btn btn-sm btn-default" disabled="disabled">instagram</button> 
                    @endif
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.contato.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop