@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Informações de Contato
        </h2>  

		{{ Form::open( array('route' => array('painel.contato.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputFrasedaSeção">Frase da Seção</label>
					<input type="text" class="form-control" id="inputFrasedaSeção" name="frase_chamada" value="{{$registro->frase_chamada}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputEmaildeContato">Email de Contato</label>
					<input type="text" class="form-control" id="inputEmaildeContato" name="email_contato" value="{{$registro->email_contato}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputFacebook">Facebook</label>
					<input type="text" class="form-control" id="inputFacebook" name="facebook" value="{{$registro->facebook}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputInstagram">Instagram</label>
					<input type="text" class="form-control" id="inputInstagram" name="instagram" value="{{$registro->instagram}}">
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop