@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Informações de Contato
        </h2>  

		<form action="{{URL::route('painel.contato.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputFrasedaSeção">Frase da Seção</label>
					<input type="text" class="form-control" id="inputFrasedaSeção" name="frase_chamada" @if(Session::has('formulario')) value="{{ Session::get('formulario.frase_chamada') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputEmaildeContato">Email de Contato</label>
					<input type="text" class="form-control" id="inputEmaildeContato" name="email_contato" @if(Session::has('formulario')) value="{{ Session::get('formulario.email_contato') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputFacebook">Facebook</label>
					<input type="text" class="form-control" id="inputFacebook" name="facebook" @if(Session::has('formulario')) value="{{ Session::get('formulario.facebook') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputInstagram">Instagram</label>
					<input type="text" class="form-control" id="inputInstagram" name="instagram" @if(Session::has('formulario')) value="{{ Session::get('formulario.instagram') }}" @endif>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop