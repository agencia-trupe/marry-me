@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Contato
        </h2>  

		<form action="{{URL::route('painel.contatosrecebidos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" @if(Session::has('formulario')) value="{{ Session::get('formulario.nome') }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" @if(Session::has('formulario')) value="{{ Session::get('formulario.telefone') }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" @if(Session::has('formulario')) value="{{ Session::get('formulario.email') }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputMensagem">Mensagem</label>
					<input type="text" class="form-control" id="inputMensagem" name="mensagem" @if(Session::has('formulario')) value="{{ Session::get('formulario.mensagem') }}" @endif >
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.contatosrecebidos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop