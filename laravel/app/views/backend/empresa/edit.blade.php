@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.empresa.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputVideo">URL do Video (Vimeo)</label>
					<input type="text" class="form-control" id="inputVideo" name="video_url" value="{{$registro->video_url}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto1">Texto - Coluna Esquerda</label>
					<textarea name="texto_coluna_1" class="form-control" id="inputTexto1" >{{$registro->texto_coluna_1 }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto2">Texto - Coluna Direita</label>
					<textarea name="texto_coluna_2" class="form-control" id="inputTexto2" >{{$registro->texto_coluna_2 }}</textarea>
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.empresa.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop