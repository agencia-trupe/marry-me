@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.empresa.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputVideo">URL do Video (Vimeo)</label>
					<input type="text" class="form-control" id="inputVideo" name="video_url" @if(Session::has('formulario')) value="{{ Session::get('formulario.video_url') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto1">Texto - Coluna Esquerda</label>
					<textarea name="texto_coluna_1" class="form-control" id="inputTexto1" >@if(Session::has('formulario')) {{ Session::get('formulario.texto_coluna_1') }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto2">Texto - Coluna Direita</label>
					<textarea name="texto_coluna_2" class="form-control" id="inputTexto2" >@if(Session::has('formulario')) {{ Session::get('formulario.texto_coluna_2') }} @endif</textarea>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.empresa.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop