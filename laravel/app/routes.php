<?php
Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::any('ajaxnewsletter', array('as' => 'cadastro.newsletter', 'uses' => 'HomeController@cadastroNewsletter'));
Route::get('quem', array('as' => 'empresa', 'uses' => 'EmpresaController@index'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::any('ajaxcontato', array('as' => 'contato.envio', 'uses' => 'ContatoController@envio'));

//Route::get('videos', array('as' => 'videos.index', 'uses' => 'VideosController@index'));
Route::get('videos', array('as' => 'videos.index', 'uses' => 'VideosController@todos'));

Route::get('videos/{categoria_slug?}', array('as' => 'videos.categorias', 'uses' => 'VideosController@categoria'));
Route::get('videos/{categoria_slug?}/{video_slug?}', array('as' => 'videos.detalhes', 'uses' => 'VideosController@detalhes'));

Route::any('busca', array('as' => 'videos.busca', 'uses' => 'VideosController@busca'));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

Route::post('ajax/AdsLogImpressao', function(){

	$dados = array(
        \Input::get('i'),
        'impressao',
        \Input::get('l'),
        \Tools::ip(),
        $_SERVER['HTTP_USER_AGENT'],
        date('Y-m-d')
	);
	\DB::insert('insert into anuncios_estatisticas (anuncios_id, acao, url, ip, user_agent, created_at) values (?, ?, ?, ?, ?, ?)', $dados);
});

Route::post('ajax/AdsLogClick', function(){

	$dados = array(
        \Input::get('i'),
        'click',
        \Input::get('l'),
        \Tools::ip(),
        $_SERVER['HTTP_USER_AGENT'],
        date('Y-m-d')
	);
	\DB::insert('insert into anuncios_estatisticas (anuncios_id, acao, url, ip, user_agent, created_at) values (?, ?, ?, ?, ?, ?)', $dados);
});

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{

	Route::get('downloadCadastros', array('as' => 'painel.downloadCadastros', function(){
    	$filename = 'cadastros_'.date('d-m-Y-H-i-s');
		\Excel::create($filename, function($excel) {
		    $excel->sheet('Usuários Cadastrados', function($sheet) {
		        $sheet->fromModel(\Newsletter::select('nome', 'email')->orderBy('nome')->get());
		    });
		})->download('csv');
    }));

    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('empresa', 'Painel\EmpresaController');
Route::resource('contato', 'Painel\ContatoController');
Route::resource('contatosrecebidos', 'Painel\ContatosRecebidosController');
Route::resource('categorias', 'Painel\CategoriasController');
Route::resource('videos', 'Painel\VideosController');
Route::resource('newsletter', 'Painel\NewsletterController');
Route::resource('anuncios', 'Painel\AnunciosController');
Route::resource('calhaus', 'Painel\CalhausController');
//NOVASROTASDOPAINEL//
});
