<?php

class Categorias extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categorias';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'asc');
    }

    public function scopeSlug($query, $slug)
    {
    	return $query->where('slug', '=', $slug);
    }

    public function scopeNotThis($query, $id)
    {
    	return $query->where('id', '!=', $id);
    }

    public function videos()
    {
        return $this->hasMany('Videos', 'categoria_id');
    }
}