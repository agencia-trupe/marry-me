<?php

class Anuncios extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'anuncios';

	protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeFiltro($query, $value)
    {
    	if($value == '') return $query;

    	return $query->where('tipo_anuncio', $value);
    }

    public function scopePublicos($query)
    {
    	return $query->where('publicar', 1);
    }

    public function scopeAtivos($query)
    {
    	return $query->where('data_entrada', '<=', date('Y-m-d'))
    				 ->where('data_saida', '>=', date('Y-m-d'));
    }

	public function scopeOrdenado($query)
    {
    	return $query->orderBy('publicar', 'desc')
    				 ->orderBy('data_entrada', 'desc');
    }

    public function impressoes(){
        return $this->hasMany('AnunciosEstatisticas', 'anuncios_id')->where('acao', 'impressao');
    }

    public function clicks(){
        return $this->hasMany('AnunciosEstatisticas', 'anuncios_id')->where('acao', 'click');
    }

    public function ctr(){
        return (sizeof($this->clicks) == 0 || sizeof($this->impressoes) == 0) ? '--' : round(sizeof($this->clicks) / sizeof($this->impressoes), 4) * 100 . "%";
    }
}