<?php

class Videos extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'videos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeSlug($query, $slug)
    {
    	return $query->where('slug', '=', $slug);
    }

    public function scopeNotThis($query, $id)
    {
    	return $query->where('id', '!=', $id);
    }

    public function scopeSemCategoria($query)
    {
    	return $query->where('categoria_id', '=', '0');
    }

    public function scopeDestaques($query)
    {
        return $query->where('destaque_home', '=', '1');
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('data', 'desc');
    }

    public function scopeBusca($query, $termo)
    {
        return empty($termo) ? $query : $query->whereRaw("MATCH(titulo,olho,texto) AGAINST(? IN BOOLEAN MODE)",[$termo]);
    }

    public function categoria(){
        return $this->belongsTo('Categorias', 'categoria_id');
    }
}