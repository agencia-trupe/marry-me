<?php

class Calhaus extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'calhaus';

	protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeFiltro($query, $value)
    {
    	if($value == '') return $query;

    	return $query->where('tipo_anuncio', $value);
    }

    public function scopePublicos($query)
    {
    	return $query->where('publicar', 1);
    }

}