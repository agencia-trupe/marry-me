(function($) {
    'use strict';

    var MM = {

        toggleLuzes: function(event) {

            event.preventDefault();

            var _this  = $(this),
                iframe = $('.video-interna-player iframe');

            if (_this.hasClass('acender')) {

                $('.overlay').fadeOut('slow', function() {
                    $(this).remove();
                    iframe.removeClass('acima');
                });

                _this.removeClass('acender').text(_this.text().replace('acender', 'apagar'));

            } else {

                $('body').prepend('<div class="overlay"></div>');
                iframe.addClass('acima');
                $('.overlay').fadeIn('slow');

                _this.addClass('acender').text(_this.text().replace('apagar', 'acender'));

            }

        },

        enviaContato: function(event) {

            event.preventDefault();
            var form     = this,
                response = $('.contato-response'),
                loading  = $('.ajax-loading');

            response.hide();
            loading.fadeIn('slow');

            $.post('ajaxcontato', {

                nome     : $('#nome').val(),
                email    : $('#email').val(),
                telefone : $('#email').val(),
                mensagem : $('#mensagem').val()

            }, function(data) {

                form.reset();
                loading.fadeOut('fast', function() {
                    response.fadeIn('slow');
                });

            });

        },

        enviaNewsletter: function(event) {

            event.preventDefault();
            var form     = $(this),
                response = $('.newsletter-response');

            $.post('ajaxnewsletter', {

                nome  : $('#nome').val(),
                email : $('#email').val()

            }, function(data) {

                form.fadeOut('fast', function() {
                    response.fadeIn('slow');
                });

            });

        },

        selecionaCarousel: function(event) {

            event.preventDefault();
            var clicked      = $(this),
                videoWrapper = $('.video-wrapper'),
                currentData  = $('.carousel-dados'),
                shareWrapper = currentData.find('.compartilhe');

            if (clicked.hasClass('active')) {
                return false;
            } else {
                var dados = clicked.data();
                $('.carousel-thumb').removeClass('active');
                clicked.addClass('active');

                videoWrapper.fadeOut('fast', function() {
                    videoWrapper.html('<iframe src="//player.vimeo.com/video/' + dados.videoid + '?color=f05a8e&portrait=0&autoplay=0&badge=0&title=0&byline=0" width="640" height="355" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
                    setTimeout(function(){videoWrapper.fadeIn('slow');}, 150);
                });

                shareWrapper.fadeOut('fast', function(){
                    $('#share-fb').attr('data-location', dados.location)
                                  .attr('href', 'https://www.facebook.com/sharer/sharer.php?u='+dados.location);

                    $('#share-tw').attr('data-location', dados.location)
                                  .attr('data-title', dados.titulo)
                                  .attr('href', 'https://twitter.com/share?url='+dados.location);
                    console.log(dados.imagem);
                    $('#share-pin').attr('data-location', dados.location)
                                   .attr('data-title', dados.titulo)
                                   .attr('data-image', dados.imagem)
                                   .attr('href', 'https://www.pinterest.com/pin/create/button/?url='+dados.location+'&media='+dados.imagem+'&description='+dados.titulo);

                    var like_button;
                    like_button = document.createElement("div");
                    like_button.setAttribute("class", "fb-like");
                    like_button.setAttribute("data-href", dados.location);
                    like_button.setAttribute("data-layout", "button_count");
                    like_button.setAttribute("data-action", "like");
                    like_button.setAttribute("data-show-faces", "false");
                    like_button.setAttribute("data-share", "false");
                    like_button.style.width = 56+"px";
                    like_button.style.height = 21+"px";

                    $('.fb-wrapper').html(like_button);

                    shareWrapper.fadeIn('fast', function(){
                        FB.XFBML.parse();
                    });
                });

                currentData.fadeOut('fast', function() {
                    $('.data').text(dados.data);
                    $('.titulo').text(dados.titulo);
                    $('.descricao').text(dados.descricao);
                    currentData.fadeIn('fast');
                });

            }

        },

        compartilhaFacebook: function(event) {
            event.preventDefault();

            var share_url;
            var has_data_location = !!$('#share-fb').attr('data-location');

            if (has_data_location)
                share_url = $('#share-fb').attr('data-location');
            else
                share_url = location.href;

            window.open('https://www.facebook.com/sharer/sharer.php?u=' + share_url, 'sharer', 'width=534,height=436');
        },

        compartilhaTwitter: function(event) {
            event.preventDefault();

            var share_url,
                share_text;

            var has_data_location = !!$('#share-tw').attr('data-location');
            var has_data_title = !!$('#share-tw').attr('data-title');

            if (has_data_location)
                share_url = $('#share-tw').attr('data-location');
            else
                share_url = location.href;

            if (has_data_title)
                share_text = $('#share-tw').attr('data-title');
            else
                share_text = $("meta[property='og:title']").attr('content');

            window.open('https://twitter.com/share?url='+share_url+'&text='+share_text, 'tweet', 'width=534,height=436');
        },

        compartilhaPinterest: function(event) {
            event.preventDefault();

            var share_url,
                share_text,
                share_image;

            var has_data_location = !!$('#share-pin').attr('data-location');
            var has_data_title = !!$('#share-pin').attr('data-title');
            var has_data_image = !!$('#share-pin').attr('data-image');

            if (has_data_location)
                share_url = $('#share-pin').attr('data-location');
            else
                share_url = location.href;

            if (has_data_title)
                share_text = $('#share-pin').attr('data-title');
            else
                share_text = $("meta[property='og:title']").attr('content');

            if (has_data_image)
                share_image = $('#share-pin').attr('data-image');
            else
                share_image = $("meta[property='og:image']").attr('content');

            window.open('https://www.pinterest.com/pin/create/button/?url='+share_url+'&media='+share_image+'&description='+share_text, 'pinterest', 'width=534,height=436');
        },

        init: function() {

            $('.luzes').on('click', MM.toggleLuzes);
            $('#contato-form').on('submit', MM.enviaContato);
            $('#newsletter-form').on('submit', MM.enviaNewsletter);
            $('.carousel-thumb').on('click', MM.selecionaCarousel);
            $('.carousel').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: false,
                touchMove: false,
                draggable: false,
                swipe: false
            });
            $('#share-fb').on('click', MM.compartilhaFacebook);
            $('#share-tw').on('click', MM.compartilhaTwitter);
            $('#share-pin').on('click', MM.compartilhaPinterest);
        }

    };

    $(document).ready( function(){
        MM.init();

        var ads_impressos = $('.adz').not('.calhau');

        if(ads_impressos.length){

            ads_impressos.each( function(index){
                var i = $(this).attr('data-track');
                $.post('ajax/AdsLogImpressao', {
                    i : i,
                    l : window.location.href
                });
            });

            ads_impressos.click( function(){
               $.post('ajax/AdsLogClick', {
                    i : $(this).attr('data-track'),
                    l : window.location.href
                });
            });
        }

    });

}(jQuery));