jQuery(function($){
    $.datepicker.regional['pt-BR'] = {
        closeText: 'Fechar',
        prevText: '&#x3c;Anterior',
        nextText: 'Pr&oacute;ximo&#x3e;',
        currentText: 'Hoje',
        monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
        'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
        'Jul','Ago','Set','Out','Nov','Dez'],
        dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

$('document').ready( function(){

    // Muda indicação de tamanho ao lado do Input File de anúncios
    $('#inputTipo').change( function(){
        $('#file_dimensions_tip').html(' - (' + $('option:selected', this).attr('data-dimensions') + ')');
    });

    // Mostra ou esconde campos de link e destino de link do cadastro de anúncios
    // Só anúncios em GIF ou JPG devem ter estes campos
    $('#inputTipoArquivo').change( function(){
        var tipo = $(this).val();

        if(tipo == 'flash'){
            $('#hasInputArquivo').show('normal');
            $('#hasInputLinks').hide('normal');
        }else{
            $('#hasInputArquivo').show('normal');
            $('#hasInputLinks').show('normal');
        }
    });

    if($('#inputTipo').length && $('#inputTipo').val() != ''){
        $('#file_dimensions_tip').html(' - (' + $('option:selected', $('#inputTipo')).attr('data-dimensions') + ')');
    }

    if($('#inputTipoArquivo').length && $('#inputTipoArquivo').val() != ''){
        var tipo = $('#inputTipoArquivo').val();

        if(tipo == 'flash'){
            $('#hasInputArquivo').show();
            $('#hasInputLinks').hide();
        }else{
            $('#hasInputArquivo').show();
            $('#hasInputLinks').show();
        }
    }

    $('[data-toggle="tooltip"]').tooltip()

    //*********************************************************************************//


  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	bootbox.confirm('Deseja Excluir o Registro?', function(result){
	      	if(result)
	        	form.submit();
	      	else
	        	$(this).modal('hide');
    	});
  	});

    $('table.table-sortable tbody').sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post('ajax/gravaOrdem', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

	$('.btn-move').click( function(e){e.preventDefault();});

	$('.datepicker').datepicker();

    if($('textarea').length)
        $('textarea.cke').ckeditor();

    $('#form-video').submit( function(e){
        var group = $('#group-url-vimeo');
        var input = group.find('input');
        var url = input.val();
        var mensagem = "<h4>A URL informada não é um link válido do <strong>Vimeo</strong></h4>";
        mensagem += "<p>";
            mensagem += "Exemplos: <br>";
            mensagem += "<em style='font-style:italic'>http://vimeo.com/6701902</em> <br>";
            mensagem += "<em style='font-style:italic'>http://player.vimeo.com/video/6701902</em> <br>";
            mensagem += "<em style='font-style:italic'>http://vimeo.com/channels/staffpicks/67019026</em>";
        mensagem += "</p>";
        var alert = "<div class='alert alert-block alert-danger' id='alerta-vimeo'><button type='button' class='close' data-dismiss='alert'>&times;</button> "+mensagem+"</div>";

        if(!check_vimeo_url(url)){
            $('#alerta-vimeo').alert('close');
            group.prepend(alert).addClass('has-error');
            window.scrollTo(0, 0);
            e.preventDefault();
            return false;
        }
    });

});

function check_vimeo_url(url) {
    var regex = new RegExp('(https?://)?(www.)?(player.)?vimeo.com/([a-z]*/)*([0-9]{6,11})[?]?.*')
    return regex.test(url);
}
